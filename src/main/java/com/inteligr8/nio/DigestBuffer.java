/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.nio.ByteBuffer;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class attempts to implement the JCA MessageDigest using the Java NIO
 * library and concepts.  It makes hashing act more like typical NIO buffer
 * operations.
 * 
 * @author brian@inteligr8.com
 * @see CipherBuffer
 * @see HashingReadableByteChannel
 * @see HashingWritableByteChannel
 */
@PublicApi
public class DigestBuffer extends AbstractBuffer {
	
	private final Logger logger = LoggerFactory.getLogger(DigestBuffer.class);
	
	private final MessageDigest digest;
	private final ByteBuffer leftoverBuffer;
	
	public DigestBuffer(DigestParameters dparams) throws NoSuchAlgorithmException {
        this.digest = dparams.getProvider() == null ? MessageDigest.getInstance(dparams.getAlgorithm()) : MessageDigest.getInstance(dparams.getAlgorithm(), dparams.getProvider());
        if (this.logger.isDebugEnabled())
        	this.logger.debug("digest algorithm: " + this.digest.getAlgorithm());
        
        this.initialized = true;
        
        this.leftoverBuffer = ByteBuffer.allocate(this.digest.getDigestLength());
        this.leftoverBuffer.flip();
	}
	
	/**
	 * An accessor for the underlying JCA digest object.
	 * 
	 * @return A JCA message digest
	 */
	public MessageDigest getMessageDigest() {
		return this.digest;
	}
	
    /**
     * This method flushes the digest cache and buffer into the specified
     * buffer.  Any subsequent call to `stream()` will fail.  If the specified
     * buffer is too small, you will need to call this method multiple times
     * to complete the flush.  You should expect to call it until it returns
     * `false`.
     * 
     * @param targetBuffer A NIO buffer ready for writing
     * @return true if more data may need flushed; false if subsequent calls are not expected
     * @throws DigestException A hashing issue has occurred
     * @see DigestBuffer#stream(ByteBuffer)
     */
	public boolean flush(ByteBuffer targetBuffer) throws DigestException {
        if (this.logger.isTraceEnabled())
            this.logger.trace("flush(" + targetBuffer.remaining() + ")");
        
        long oldTargetBytesWritten = this.targetBytesWritten;
        
        // leftovers exist; must eat
        if (this.leftoverBuffer.remaining() > 0) {
            if (this.logger.isTraceEnabled())
                this.logger.trace("flush(" + targetBuffer.remaining() + "): draining leftovers");
            
            this.targetBytesWritten += this.copyBuffer(this.leftoverBuffer, targetBuffer);
            
            if (this.leftoverBuffer.remaining() > 0)
            	return true;
        // otherwise no more data anywhere
        } else {
            if (this.logger.isTraceEnabled())
                this.logger.trace("flush(" + targetBuffer.remaining() + "): nothing to flush");
        }
		
		if (!this.finished) {
			// ready for writing
			this.leftoverBuffer.clear();
			
        	byte[] bytes = this.digest.digest();
        	this.leftoverBuffer.put(bytes);
    		this.finished = true;
			
			// ready for reading
			this.leftoverBuffer.flip();
			
			this.targetBytesWritten += this.copyBuffer(this.leftoverBuffer, targetBuffer);
			
			if (this.leftoverBuffer.hasRemaining())
				return true;
		}
		
		return oldTargetBytesWritten < this.targetBytesWritten;
    }
	
	/**
	 * This method streams hashing from a source NIO buffer to a target NIO
	 * buffer.  After streaming all the source, you will start the `flush`
     * phase.
	 * 
	 * @param sourceBuffer A NIO buffer ready for reading
	 * @see DigestBuffer#flush(ByteBuffer)
	 */
	public void stream(ByteBuffer sourceBuffer) {
		if (sourceBuffer == null)
			throw new IllegalArgumentException();
		if (this.finished)
			throw new IllegalStateException("There should no longer be a source to this digest");
		
        if (this.logger.isTraceEnabled())
            this.logger.trace("stream(" + sourceBuffer.remaining() + ")");

        if (!this.started)
            this.started = true;
        
        int bytesToRead = sourceBuffer.remaining();
        this.digest.update(sourceBuffer);
        this.sourceBytesRead += bytesToRead - sourceBuffer.remaining();
	}
	
}
