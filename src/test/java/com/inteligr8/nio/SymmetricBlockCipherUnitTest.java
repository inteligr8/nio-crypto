package com.inteligr8.nio;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.Key;
import java.util.Random;

import javax.crypto.Cipher;

import org.junit.Assert;
import org.junit.Test;

public class SymmetricBlockCipherUnitTest extends AbstractCipherUnitTest {
	
	@Override
	public String getDefaultAlgorithm() {
		return "AES";
	}

	@Override
	public int getDefaultKeySize() {
		return 128;
	}
	
	@Test
	public void testEmptyFlushEmptyTargetEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);
		
		ByteBuffer buffer = ByteBuffer.allocate(0);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyFlushEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);
		
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());

		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiFlushEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		int chunkSize = this.getDefaultKeySizeInBytes() / 2 - 1;
		ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize * 2L, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(2, buffer.position());

		buffer.clear();
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(0, buffer.position());

		Assert.assertFalse(cipher.flush(buffer));
	}
	
	@Test
	public void testEmptyFlushEmptyTargetDecryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);
		
		ByteBuffer buffer = ByteBuffer.allocate(0);
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyFlushDecryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);

		ByteBuffer buffer = ByteBuffer.allocate(1024);
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, buffer.position());
	}
	
	@Test
	public void testEmptyMultiFlushDecryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);

		int chunkSize = this.getDefaultKeySizeInBytes() / 2 - 1;
		ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, buffer.position());

		Assert.assertFalse(cipher.flush(buffer));
	}
	
	@Test
	public void testEmptyStreamEmptyTargetEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyStreamEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, targetBuffer.position());
	}
	
	@Test
	public void testEmptyMultiStreamEncryption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, targetBuffer.position());

		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
	}
	
	@Test
	public void testEmptyStreamEmptyTargetDecyption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, targetBuffer.position());
	}
	
	@Test
	public void testEmptyStreamDecyption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, targetBuffer.position());

		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
	}
	
	@Test
	public void testEmptyMultiStreamDecyption() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.DECRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		Assert.assertEquals(0, targetBuffer.position());

		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStreamAfterFlush() throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertTrue(cipher.flush(targetBuffer));
		cipher.stream(sourceBuffer, targetBuffer);
		Assert.fail();
	}
	
	@Test
	public void testEmptyEncryption() throws Exception {
		this.testSubBlockByteEncryption(0);
	}
	
	@Test
	public void test1ByteEncryption() throws Exception {
		this.testSubBlockByteEncryption(1);
	}
	
	@Test
	public void test5ByteEncryption() throws Exception {
		this.testSubBlockByteEncryption(5);
	}
	
	@Test
	public void test15ByteEncryption() throws Exception {
		this.testSubBlockByteEncryption(15);
	}
	
	@Test
	public void test16ByteEncryption() throws Exception {
		this.testBlockByteEncryption(16);
	}
	
	@Test
	public void test17ByteEncryption() throws Exception {
		this.testBlockByteEncryption(17);
	}
	
	@Test
	public void test31ByteEncryption() throws Exception {
		this.testBlockByteEncryption(31);
	}
	
	@Test
	public void test32ByteEncryption() throws Exception {
		this.testBlockByteEncryption(32);
	}
	
	@Test
	public void test100ByteEncryption() throws Exception {
		this.testBlockByteEncryption(100);
	}
	
	@Test
	public void test10000ByteEncryption() throws Exception {
		this.testBlockByteEncryption(10000);
	}
	
	private void testSubBlockByteEncryption(int bytes) throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		int chunkSize = this.getDefaultKeySizeInBytes() / 2;
		
		ByteBuffer sourceBuffer = ByteBuffer.allocate(bytes);
		new Random().nextBytes(sourceBuffer.array());
		
		ByteBuffer targetBuffer = ByteBuffer.allocate(chunkSize);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(0, cipher.getTotalBytesWritten());
		Assert.assertEquals(bytes, sourceBuffer.position());
		Assert.assertEquals(0, targetBuffer.position());
		
		Assert.assertTrue(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize, cipher.getTotalBytesWritten());
		Assert.assertEquals(bytes, sourceBuffer.position());
		Assert.assertEquals(chunkSize, targetBuffer.position());
		
		targetBuffer.clear();
		Assert.assertTrue(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, targetBuffer.position());

		targetBuffer.clear();
		Assert.assertFalse(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	private void testBlockByteEncryption(int bytes) throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, key);

		int chunkSize = this.getDefaultKeySizeInBytes();
		
		ByteBuffer sourceBuffer = ByteBuffer.allocate(bytes);
		new Random().nextBytes(sourceBuffer.array());
		
		ByteBuffer targetBuffer = ByteBuffer.allocate(chunkSize);
		boolean done = false;
		while (!done) {
			done = !cipher.stream(sourceBuffer, targetBuffer);
			Assert.assertEquals(done ? 0 : chunkSize, targetBuffer.position());
			targetBuffer.clear();
		}

		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(bytes / this.getDefaultKeySizeInBytes() * this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertFalse(sourceBuffer.hasRemaining());
		
		done = false;
		while (!done)
			done = !cipher.flush(targetBuffer);

		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(bytes / this.getDefaultKeySizeInBytes() * this.getDefaultKeySizeInBytes() + this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void encryptDecryptShort() throws Exception {
		String plainText = "Short!";
		this.encryptDecrypt(plainText);
	}
	
	@Test
	public void encryptDecryptLong() throws Exception {
		String plainText = "Here is some text that has a respectable amount of length to it";
		this.encryptDecrypt(plainText);
	}
	
	private void encryptDecrypt(String plainText) throws Exception {
		Key key = this.generateSecretKey();
		CipherBuffer encryptionCipher = this.createCipher(Cipher.ENCRYPT_MODE, key);
		CipherBuffer decryptionCipher = this.createCipher(Cipher.DECRYPT_MODE, key, encryptionCipher);

		ByteBuffer sourceBuffer = ByteBuffer.wrap(plainText.getBytes(Charset.defaultCharset()));
		ByteBuffer encryptedBuffer = ByteBuffer.allocate(1024 * 1024); // plenty of size
		ByteBuffer decryptedBuffer = ByteBuffer.allocate(1024 * 1024); // plenty of size
		while (encryptionCipher.stream(sourceBuffer, encryptedBuffer))
			;
		while (encryptionCipher.flush(encryptedBuffer))
			;
		
		Assert.assertEquals(plainText.length(), encryptionCipher.getTotalBytesRead());
		Assert.assertTrue(encryptionCipher.getTotalBytesRead() < encryptionCipher.getTotalBytesWritten());
		
		encryptedBuffer.flip(); // ready for reading
		Assert.assertEquals(encryptionCipher.getTotalBytesWritten(), encryptedBuffer.remaining());
		
		while (decryptionCipher.stream(encryptedBuffer, decryptedBuffer))
			;
		while (decryptionCipher.flush(decryptedBuffer))
			;
		
		decryptedBuffer.flip(); // ready for reading

		Assert.assertEquals(encryptionCipher.getTotalBytesWritten(), decryptionCipher.getTotalBytesRead());
		Assert.assertEquals(plainText.length(), decryptionCipher.getTotalBytesWritten());
		Assert.assertEquals(plainText, new String(decryptedBuffer.array(), decryptedBuffer.position(), decryptedBuffer.remaining(), Charset.defaultCharset()));
	}

}
