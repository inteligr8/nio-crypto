/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.security.Provider;

/**
 * This class is a collection of accessors and modifiers for the parameters
 * used by the message digest classes in this library.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class DigestParameters {
	
	private String algorithm;
	private Provider provider;
	
	/**
	 * @param algorithm A hashing algorithm
	 */
	public DigestParameters(String algorithm) {
		this(algorithm, null);
	}
	
	/**
	 * @param algorithm A hashing algorithm
	 * @param provider A JCA provider
	 */
	public DigestParameters(String algorithm, Provider provider) {
		this.algorithm = algorithm;
		this.provider = provider;
	}
	
	public String getAlgorithm() {
		return this.algorithm;
	}
	
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	
	public Provider getProvider() {
		return this.provider;
	}
	
	public DigestParameters setProvider(Provider provider) {
		this.provider = provider;
		return this;
	}
	
}
