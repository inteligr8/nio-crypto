/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

/**
 * This interface defines accessors for metered information on the decryption
 * channels implemented in this library.
 * 
 * @author brian@inteligr8.com
 * @see EncryptionMetered
 */
@PublicApi
public interface DecryptionMetered {

	/**
	 * An accessor for the total number of bytes read from encrypted source
	 * buffers or channel.
	 * 
	 * The total number of bytes read only indirectly affects the total number
	 * of bytes written.  That impact is dictated by the context of the buffer
	 * implementation.  If there is caching, writing will appear behind.  If
	 * there is padding, writing may appear ahead.  It is best to never assume
	 * any predictable relationship between one and the other.
	 * 
	 * @return A number of bytes; never negative
	 * @see AbstractBuffer#getTotalBytesRead()
	 * @see DecryptionMetered#getDecryptedBytesWritten()
	 */
	long getEncryptedBytesRead();

	/**
	 * An accessor for the total number of bytes written to decrypted target
	 * buffers or channel.
	 * 
	 * The total number of bytes written is only indirectly dependent on the
	 * total number of bytes read.  That dependency is dictated by the context
	 * of the buffer implementation.  If there is caching, writing will appear
	 * behind.  If there is padding, writing may appear ahead.  It is best to
	 * never assume any predictable relationship between one and the other.
	 * 
	 * @return A number of bytes; never negative
	 * @see AbstractBuffer#getTotalBytesWritten()
	 * @see DecryptionMetered#getEncryptedBytesRead()
	 */
	long getDecryptedBytesWritten();
	
}
