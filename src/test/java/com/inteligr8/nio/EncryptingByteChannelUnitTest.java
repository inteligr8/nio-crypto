package com.inteligr8.nio;

import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;

public class EncryptingByteChannelUnitTest extends AbstractCryptoByteChannelUnitTest {
    
    public void test(String text, String hex) throws Exception {
        int keySizeInBytes = this.getKeySizeInBytes();
        Assert.assertEquals(2 * (text.length() / keySizeInBytes * keySizeInBytes + keySizeInBytes), hex.length());
        
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        EncryptingWritableByteChannel ebchannel = new EncryptingWritableByteChannel(bbchannel, new EncryptingCipherParameters(this.getKey(), "AES/CBC/PKCS5Padding", new byte[this.getKeySizeInBytes()]));
        try {
            int bytesWritten = ebchannel.write(this.getCharset().encode(text));
            if (text.length() == 0) Assert.assertEquals(0, bytesWritten);
            else Assert.assertEquals(text.length() / keySizeInBytes * keySizeInBytes, bytesWritten);
        } finally {
            ebchannel.close();
        }

        Assert.assertEquals(text.length(), ebchannel.getDecryptedBytesRead());
        Assert.assertEquals(hex.length() / 2, ebchannel.getEncryptedBytesWritten());
        
        ByteBuffer bbuffer = ByteBuffer.allocate(1024);
        bbchannel.read(bbuffer);
        bbuffer.flip();
        
        byte[] bytes = new byte[bbuffer.remaining()];
        bbuffer.get(bytes);
        Assert.assertEquals(hex, new String(Hex.encodeHex(bytes)));
        Assert.assertEquals(hex.length() / 2, bytes.length);
    }

}
