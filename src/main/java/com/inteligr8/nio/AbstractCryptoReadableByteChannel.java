/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

abstract class AbstractCryptoReadableByteChannel extends AbstractCryptoByteChannel implements ReadableByteChannel {
	
	protected final ReadableByteChannel rbchannel;
	
	private int chunkSize;
	protected ByteBuffer readBuffer;
	
	public AbstractCryptoReadableByteChannel(ReadableByteChannel rbchannel, CipherParameters cparams)
	throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		super(rbchannel, cparams);
		this.rbchannel = rbchannel;
		this.setChunkSize(16384);
	}
	
	/**
	 * This method overrides the default chunk size for reading this channel
	 * into buffers.  The lower the value, the less memory usage.  The higher
	 * the value, the higher performance.
	 * 
	 * @param chunkSize A number of bytes; defaults to 16KB
	 */
	public void setChunkSize(int chunkSize) {
		if (this.readBuffer != null && this.readBuffer.hasRemaining())
			throw new IllegalStateException("You cannot change the buffer size while the buffer is actively used");
		this.chunkSize = chunkSize;
		this.readBuffer = ByteBuffer.allocate(this.chunkSize);
		this.readBuffer.flip(); // ready for reading by default
	}
    
    /**
     * This method reads and encrypts or decrypts the underlying channel until
     * it is either empty or the specified buffer is full.
     * 
     * @param buffer A NIO buffer ready for writing
     * @return The number of bytes read from the underlying channel; -1 for EOF; 0 is possible and should be re-read
     * @throws IOException An I/O or crypto exception occurred
     */
    protected int _read(ByteBuffer buffer) throws IOException, IllegalBlockSizeException, BadPaddingException {
    	int totalBytesRead = 0;
    	int oldBufferPosition = buffer.position();
    	
    	this.readBuffer.compact(); // ready for writing
    	int bytesRead = this.rbchannel.read(this.readBuffer);
		this.readBuffer.flip();    // ready for reading
		
		if (!Status.Finished.equals(this.getCipher().getStatus())) {
			do {
				if (bytesRead > 0)
					totalBytesRead += bytesRead;
				
				while (buffer.hasRemaining() && this.getCipher().stream(this.readBuffer, buffer))
					;
				
		    	this.readBuffer.compact(); // ready for writing
		    	bytesRead = this.rbchannel.read(this.readBuffer);
				this.readBuffer.flip();    // ready for reading
			} while (bytesRead > 0);
		}

    	if (bytesRead > 0)
    		totalBytesRead += bytesRead;
    	
    	if (bytesRead < 0 && buffer.hasRemaining()) {
    		while (buffer.hasRemaining() && this.getCipher().flush(buffer))
    			;
    	}
    	
    	if (totalBytesRead > 0)
    		// something read from underlying channel
    		return totalBytesRead;
    	if (oldBufferPosition < buffer.position())
    		// something read from buffer or cipher cache
			return 0;
    	if (buffer.hasRemaining())
	    	// apparently nothing left to do
	    	return -1;
    	// could be done, but let the next all determine that
    	return 0;
    }

}
