package com.inteligr8.nio;

import java.security.InvalidParameterException;
import java.security.Key;

import javax.crypto.Cipher;

import org.junit.Assert;
import org.junit.Test;

public class CommonCipherUnitTest extends AbstractCipherUnitTest {
	
	@Override
	public String getDefaultAlgorithm() {
		return "AES";
	}
	
	@Override
	public int getDefaultKeySize() {
		return 128;
	}
	
	@Test(expected = InvalidParameterException.class)
	public void testInvalidMode() throws Exception {
		Key key = this.generateSecretKey(this.getDefaultTransformation(), this.getDefaultKeySize());
		new CipherBuffer(new CipherParameters(1359823095, key));
		Assert.fail();
	}
	
	@Test(expected = NullPointerException.class)
	public void testNullKey() throws Exception {
		new CipherBuffer(new CipherParameters(Cipher.ENCRYPT_MODE, null));
		Assert.fail();
	}

}
