/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.Flushable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;

import javax.crypto.NoSuchPaddingException;

import com.inteligr8.nio.CipherParameters.IVSource;

/**
 * This class embeds a random initialization vector at the start of the
 * encrypted content.  Otherwise it acts identical to the
 * encrypting writable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see EncryptingWritableByteChannel
 */
@PublicApi
public class IVEncryptingWritableByteChannel implements WritableByteChannel, Flushable, EncryptionMetered {
    
	private final WritableByteChannel wbchannel;
    private final EncryptingWritableByteChannel ewbchannel;
    private boolean wroteIV = false;

    /**
     * @param wbchannel A writable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVEncryptingWritableByteChannel(WritableByteChannel wbchannel, Key key, String transformation)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
    	this.wbchannel = wbchannel;
        this.ewbchannel = new EncryptingWritableByteChannel(wbchannel, new EncryptingCipherParameters(key, transformation, IVSource.Generated));
    }

    /**
     * @param wbchannel A writable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @param iv A byte array of the initialization vector
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVEncryptingWritableByteChannel(WritableByteChannel wbchannel, Key key, String transformation, byte[] iv)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
    	this.wbchannel = wbchannel;
        this.ewbchannel = new EncryptingWritableByteChannel(wbchannel, new EncryptingCipherParameters(key, transformation, iv));
    }

    /**
     * @param wbchannel A writable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @param provider A JCA provider
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVEncryptingWritableByteChannel(WritableByteChannel wbchannel, Key key, String transformation, Provider provider)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
    	this.wbchannel = wbchannel;
        this.ewbchannel = new EncryptingWritableByteChannel(wbchannel, new EncryptingCipherParameters(key, transformation, IVSource.Generated, provider));
    }

    /**
     * @param wbchannel A writable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @param iv A byte array of the initialization vector
     * @param provider A JCA provider
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVEncryptingWritableByteChannel(WritableByteChannel wbchannel, Key key, String transformation, byte[] iv, Provider provider)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
    	this.wbchannel = wbchannel;
        this.ewbchannel = new EncryptingWritableByteChannel(wbchannel, new EncryptingCipherParameters(key, transformation, iv, provider));
    }

    /**
     * @see EncryptingWritableByteChannel#close()
     */
    @Override
    public void close() throws IOException {
        this.ewbchannel.close();
    }

    /**
     * @see EncryptingWritableByteChannel#isOpen()
     */
    @Override
    public boolean isOpen() {
        return this.ewbchannel.isOpen();
    }
    
    /**
     * @see EncryptingWritableByteChannel#flush()
     */
    @Override
    public void flush() throws IOException {
        this.ewbchannel.flush();
    }

    /**
     * @see EncryptionMetered#getDecryptedBytesRead()
     */
    @Override
    public long getDecryptedBytesRead() {
        return this.ewbchannel.getDecryptedBytesRead();
    }

    /**
     * @see EncryptionMetered#getEncryptedBytesWritten()
     */
    @Override
    public long getEncryptedBytesWritten() {
        long bytes = this.ewbchannel.getEncryptedBytesWritten();
        if (this.wroteIV)
        	bytes += this.ewbchannel.getCipher().getInitializationVector().length;
        return bytes;
    }

    /**
     * This method will write the configured or generated IV to the raw
     * writable byte buffer, configure the cipher to use that IV, and then
     * encrypt the rest of the writable byte buffer.
     * 
     * @param buffer A NIO buffer ready for reading
     * @return The number of bytes written to the underlying channel; 0 is possible
     * @see EncryptingWritableByteChannel#write(ByteBuffer)
     */
    @Override
    public int write(ByteBuffer buffer) throws IOException {
        int bytes = 0;
        if (!this.wroteIV) {
            ByteBuffer iv = ByteBuffer.wrap(this.ewbchannel.getCipher().getInitializationVector());
            bytes += this.wbchannel.write(iv);
            this.wroteIV = true;
        }

        bytes += this.ewbchannel.write(buffer);
        return bytes;
    }

}
