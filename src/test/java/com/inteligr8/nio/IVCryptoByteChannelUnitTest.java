package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Test;

public class IVCryptoByteChannelUnitTest extends AbstractCryptoByteChannelUnitTest {
    
    public void test(String text, String hex) throws Exception {
        this.text(text);
        this.chunk(text, 512);
        this.chunk(text, 5);
    }
    
    @Test
    public void partialLessThan8b() throws Exception {
        this.partialText("5Hello", "5");
    }
    
    @Test
    public void partialMoreThan8b() throws Exception {
        this.partialText("12Hello World!", "12");
    }
    
    private void text(String text) throws IOException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        int keySizeInBytes = this.getKeySizeInBytes();
        
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        IVEncryptingWritableByteChannel ebchannel = new IVEncryptingWritableByteChannel(bbchannel, this.getKey(), "AES/CBC/PKCS5Padding");
        try {
            int bytesWritten = ebchannel.write(this.getCharset().encode(text));
            if (text.length() == 0) Assert.assertEquals(keySizeInBytes, bytesWritten);
            else Assert.assertEquals(text.length() / keySizeInBytes * keySizeInBytes + keySizeInBytes, bytesWritten);
        } finally {
            ebchannel.close();
        }
        
        Assert.assertTrue(bbchannel.size() >= text.length());
        
        ByteBuffer bbuffer = ByteBuffer.allocate(1024);
        IVDecryptingReadableByteChannel dbchannel = new IVDecryptingReadableByteChannel(bbchannel, this.getKey(), "AES/CBC/PKCS5Padding");
        try {
        	int bytesRead = dbchannel.read(bbuffer);
            Assert.assertEquals(text.length() / keySizeInBytes * keySizeInBytes + 2 * keySizeInBytes, bytesRead);
        	Assert.assertEquals(-1, dbchannel.read(bbuffer));
        } finally {
            dbchannel.close();
        }
        
        bbuffer.flip();
        Assert.assertEquals(text, this.getCharset().decode(bbuffer).toString());
    }
    
    private void chunk(String text, int chunkSize) throws IOException, InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        ByteBuffer bbuffer = this.getCharset().encode(text);
        int realLimit = bbuffer.limit();
        bbuffer.limit(Math.min(chunkSize, realLimit));
        
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        IVEncryptingWritableByteChannel ebchannel = new IVEncryptingWritableByteChannel(bbchannel, getKey(), "AES/CBC/PKCS5Padding");
        try {
            while (bbuffer.hasRemaining()) {
            	ebchannel.write(bbuffer);
                bbuffer.limit(Math.min(bbuffer.limit() + chunkSize, realLimit));
            }
        } finally {
            ebchannel.close();
        }
        
        Assert.assertTrue(bbchannel.size() >= text.length());
        
        bbuffer = ByteBuffer.allocate(1024);
        realLimit = bbuffer.limit();
        bbuffer.limit(Math.min(chunkSize, realLimit));
        IVDecryptingReadableByteChannel dbchannel = new IVDecryptingReadableByteChannel(bbchannel, getKey(), "AES/CBC/PKCS5Padding");
        try {
            int bytesRead = 1;
            while (bytesRead >= 0) {
                bytesRead = dbchannel.read(bbuffer);
                bbuffer.limit(Math.min(bbuffer.limit() + chunkSize, realLimit));
            }
        } finally {
            dbchannel.close();
        }
        
        bbuffer.flip();
        Assert.assertEquals(text, this.getCharset().decode(bbuffer).toString());
    }
    
    private void partialText(String text, String startsWith) throws Exception {
        int keySizeInBytes = this.getKeySizeInBytes();
        
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        IVEncryptingWritableByteChannel ebchannel = new IVEncryptingWritableByteChannel(bbchannel, this.getKey(), "AES/CBC/PKCS5Padding");
        try {
            int bytesWritten = ebchannel.write(this.getCharset().encode(text));
            Assert.assertEquals(text.length() / keySizeInBytes * keySizeInBytes + keySizeInBytes, bytesWritten);
        } finally {
            ebchannel.close();
        }
        
        Assert.assertTrue(bbchannel.size() >= text.length());
        
        ByteBuffer bbuffer = ByteBuffer.allocate(startsWith.length());
        IVDecryptingReadableByteChannel dbchannel = new IVDecryptingReadableByteChannel(bbchannel, this.getKey(), "AES/CBC/PKCS5Padding");
        try {
            int bytesRead = dbchannel.read(bbuffer);
            Assert.assertEquals(startsWith.length() / keySizeInBytes * keySizeInBytes + 2 * keySizeInBytes, bytesRead);
        } finally {
            dbchannel.close();
        }
        
        bbuffer.flip();
        Assert.assertEquals(startsWith, this.getCharset().decode(bbuffer).toString());
    }

}
