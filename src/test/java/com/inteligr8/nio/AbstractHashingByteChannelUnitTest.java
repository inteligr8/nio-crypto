package com.inteligr8.nio;

import java.io.File;
import java.nio.channels.ByteChannel;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import org.junit.Test;

abstract class AbstractHashingByteChannelUnitTest {
    
    private final Charset charset = Charset.forName("utf-8");
    
    public Charset getCharset() {
        return this.charset;
    }
    
    @Test
    public void empty() throws Exception {
        this.test("SHA-256", "", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
    }
    
    @Test
    public void lessThan8b() throws Exception {
        this.test("SHA-1", "Hello", "f7ff9e8b7bb2e09b70935a5d785e0cc5d9d0abf0");
    }
    
    @Test
    public void exactly8b() throws Exception {
        this.test("MD5", "Hi there", "d9385462d3deff78c352ebb3f941ce12");
    }
    
    @Test
    public void between8b16b() throws Exception {
        this.test("SHA-512", "Hello World!", "861844d6704e8573fec34d967e20bcfef3d424cf48be04e6dc08f2bd58c729743371015ead891cc3cf1c9d34b49264b510751b1ff9e537937bc46b5d6ff4ecc8");
    }
    
    @Test
    public void exactly16b() throws Exception {
        this.test("sha-384", "Hello over there", "3f8b70c00fed6d8c44434508acb99671f15118854880d50d1f997951a4e40d99fb4451e8c23975f02c274aacf6253570");
    }
    
    @Test
    public void between16b32b() throws Exception {
        this.test("sha-256", "Howdy y'all compadres!", "f0e28418a2ccab2254997b1a78537e900e0368e6472470e99a1e858e04cf42d3");
    }
    
    @Test
    public void moreThan32b() throws Exception {
        this.test("sha-256", "Hello to all and to all a good night", "277f6a3626c62240cebced1ae279e3cc49bf1cf3a6ffdc0ae5709033e4e3044e");
    }
    
    @Test
    public void javaDigestBuffer() throws Exception {
        this.test("sha-256", new File("src/main/java/com/inteligr8/nio/DigestBuffer.java"), "c82a52583fc3319b117c6bee33781ef6cafb5058e77236271dbb636ba08ee3f1");
    }
    
    public void test(String algorithm, String text, String hex) throws Exception {
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        bbchannel.write(this.getCharset().encode(text));
        this.test(algorithm, bbchannel, hex);
    }
    
    public void test(String algorithm, File file, String hex) throws Exception {
    	FileChannel fchannel = FileChannel.open(file.toPath());
    	this.test(algorithm, fchannel, hex);
    }
    
    public abstract void test(String algorithm, ByteChannel bchannel, String hex) throws Exception;

}
