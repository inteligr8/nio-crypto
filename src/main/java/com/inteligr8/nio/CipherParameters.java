/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.security.Key;
import java.security.Provider;

/**
 * This class is a collection of accessors and modifiers for the parameters
 * used by the cipher classes in this library.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class CipherParameters {
	
	/**
	 * A enumeration of the possible sources of initialization vectors.
	 */
	@PublicApi
	public enum IVSource {
		
		/**
		 * There is not IV expected.
		 */
		None,
		
		/**
		 * The IV intends to be generated automatically.  This is only useful
		 * for encryption; not decryption.
		 */
		Generated,
		
		/**
		 * The IV intends to be defined after construction of the class.  This
		 * is useful if you will know the IV but you don't know it yet.  This
		 * is particularly useful for decryption with embedded IVs.
		 */
		Deferred,
		
		/**
		 * The IV is provided during the construction of the class.
		 */
		Provided
	}
	
    private int cipherMode;
	private Key key;
	private String transformation;
	private boolean generateIV = false;
	private boolean deferIV = false;
	private byte[] iv;
	private Provider provider;
	
	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 */
	public CipherParameters(int cipherMode, Key key) {
		this(cipherMode, key, null, IVSource.None, null, null);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 */
	public CipherParameters(int cipherMode, Key key, String transformation) {
		this(cipherMode, key, transformation, IVSource.None, null, null);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param ivSource The intended source of the optional IV; use the byte[] for IVSource.Provided
	 */
	public CipherParameters(int cipherMode, Key key, String transformation, IVSource ivSource) {
		this(cipherMode, key, transformation, ivSource, null, null);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param initializationVector A byte array of the optional IV
	 */
	public CipherParameters(int cipherMode, Key key, String transformation, byte[] initializationVector) {
		this(cipherMode, key, transformation, IVSource.Provided, initializationVector, null);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param provider A JCA provider
	 */
	public CipherParameters(int cipherMode, Key key, Provider provider) {
		this(cipherMode, key, null, IVSource.None, null, provider);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param provider A JCA provider
	 */
	public CipherParameters(int cipherMode, Key key, String transformation, Provider provider) {
		this(cipherMode, key, transformation, IVSource.None, null, provider);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param ivSource The intended source of the optional IV; use the byte[] for IVSource.Provided
	 * @param provider A JCA provider
	 */
	public CipherParameters(int cipherMode, Key key, String transformation, IVSource ivSource, Provider provider) {
		this(cipherMode, key, transformation, ivSource, null, provider);
	}

	/**
	 * @param cipherMode Cipher#DECRYPT_MODE or Cipher#ENCRYPT_MODE
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param initializationVector A byte array of the optional IV
	 * @param provider A JCA provider
	 */
	public CipherParameters(int cipherMode, Key key, String transformation, byte[] initializationVector, Provider provider) {
		this(cipherMode, key, transformation, IVSource.Provided, initializationVector, provider);
	}
	
	private CipherParameters(int cipherMode, Key key, String transformation, IVSource ivSource, byte[] initializationVector, Provider provider) {
		this.cipherMode = cipherMode;
		this.key = key;
		this.transformation = transformation;
		this.provider = provider;
		
		switch (ivSource) {
			case Generated:
				this.generateIV = true;
			case Deferred:
				this.deferIV = true;
			case Provided:
				this.iv = initializationVector;
			case None:
			default:
		}
	}
	
	public int getCipherMode() {
		return this.cipherMode;
	}
	
	public CipherParameters setCipherMode(int cipherMode) {
		this.cipherMode = cipherMode;
		return this;
	}
	
	public Key getKey() {
		return this.key;
	}
	
	public CipherParameters setKey(Key key) {
		this.key = key;
		return this;
	}
	
	public String getTransformation() {
		return this.transformation;
	}
	
	public CipherParameters setTransformation(String transformation) {
		this.transformation = transformation;
		return this;
	}
	
	public byte[] getInitializationVector() {
		return this.iv;
	}
	
	public CipherParameters setInitializationVector(byte[] initializationVector) {
		this.iv = initializationVector;
		return this;
	}
	
	public boolean isGenerateInitializationVector() {
		return this.generateIV;
	}
	
	public CipherParameters setGenerateInitializationVector(boolean generateInitializationVector) {
		this.generateIV = generateInitializationVector;
		return this;
	}
	
	public boolean isDeferInitializationVector() {
		return this.deferIV;
	}
	
	public CipherParameters setDeferInitializationVector(boolean deferInitializationVector) {
		this.deferIV = deferInitializationVector;
		return this;
	}
	
	public Provider getProvider() {
		return this.provider;
	}
	
	public CipherParameters setProvider(Provider provider) {
		this.provider = provider;
		return this;
	}
	
}
