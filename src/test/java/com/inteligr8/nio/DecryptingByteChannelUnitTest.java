package com.inteligr8.nio;

import java.nio.ByteBuffer;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;

public class DecryptingByteChannelUnitTest extends AbstractCryptoByteChannelUnitTest {
    
    public void test(String text, String hex) throws Exception {
        Assert.assertEquals(2 * this.roundUpToKeySize(text.length()), hex.length());
        
        ByteBuffer bbuffer = ByteBuffer.allocate(1024);
        
        ByteBufferChannel bbchannel = new ByteBufferChannel(1024);
        bbchannel.write(ByteBuffer.wrap(Hex.decodeHex(hex.toCharArray())));
        DecryptingReadableByteChannel dbchannel = new DecryptingReadableByteChannel(bbchannel, new DecryptingCipherParameters(this.getKey(), "AES/CBC/PKCS5Padding", new byte[this.getKeySizeInBytes()]));
        try {
            int bytesRead = dbchannel.read(bbuffer);
            Assert.assertEquals(this.roundUpToKeySize(text.length()), bytesRead);
        } finally {
            dbchannel.close();
        }

        Assert.assertEquals(hex.length() / 2, dbchannel.getEncryptedBytesRead());
        Assert.assertEquals(text.length(), dbchannel.getDecryptedBytesWritten());
        
        bbuffer.flip();
        Assert.assertEquals(text.length(), bbuffer.remaining());
        Assert.assertEquals(text, this.getCharset().decode(bbuffer).toString());
    }
    
    protected int roundUpToKeySize(int bytes) {
    	int keySize = this.getKeySizeInBytes();
    	return bytes / keySize * keySize + keySize;
    }

}
