/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * This class implements a NIO writable byte channel that encrypts the buffers
 * as it is written to an underlying writable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see EncryptingReadableByteChannel
 * @see DecryptingWritableByteChannel
 */
@PublicApi
public class EncryptingWritableByteChannel extends AbstractCryptoWritableByteChannel implements EncryptionMetered {
	
	public EncryptingWritableByteChannel(WritableByteChannel wbchannel, EncryptingCipherParameters cparams)
	throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		super(wbchannel, cparams);
	}

	/**
	 * @see EncryptionMetered#getDecryptedBytesRead()
	 */
	@Override
    public long getDecryptedBytesRead() {
        return this.getTotalBytesRead();
    }

	/**
	 * @see EncryptionMetered#getEncryptedBytesWritten()
	 */
	@Override
    public long getEncryptedBytesWritten() {
        return this.getTotalBytesWritten();
    }
    
    /**
     * This method encrypts and writes to the underlying channel until the
     * specified buffer is empty.
     * 
     * @param buffer A NIO buffer ready for reading
     * @return The number of bytes written to the underlying channel; never negative
     * @throws IOException An I/O or crypto exception occurred
     * @see EncryptingWritableByteChannel#flush()
     */
    @Override
    public int write(ByteBuffer buffer) throws IOException {
		try {
			return this._write(buffer);
		} catch (IllegalBlockSizeException ibse) {
			throw new IOException("An paddding issue was discovered in the encrypted channel", ibse);
		} catch (BadPaddingException bpe) {
			// only for decryption
			throw new AssertionError("This should never happen", bpe);
		}
    }
    
    /**
     * This method encrypts and writes to the underlying channel until the
     * internal caches and buffers are empty.
     * 
     * @throws IOException An I/O or crypto exception occurred
     * @see EncryptingWritableByteChannel#write(ByteBuffer)
     */
    @Override
    public void flush() throws IOException {
		try {
			this._flush();
		} catch (IllegalBlockSizeException ibse) {
			throw new IOException("An paddding issue was discovered in the encrypted channel", ibse);
		} catch (BadPaddingException bpe) {
			// only for decryption
			throw new AssertionError("This should never happen", bpe);
		}
    }

}
