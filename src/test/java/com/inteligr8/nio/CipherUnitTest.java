package com.inteligr8.nio;

import java.nio.ByteBuffer;
import java.security.Key;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CipherUnitTest {
	
	private Cipher cipher;
	
	@Before
	public void init() throws Exception {
		Key key = KeyGenerator.getInstance("AES").generateKey();
		this.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
	}
	
	@Test
	public void blockSizeChunking() throws Exception {
		ByteBuffer targetBuffer = ByteBuffer.allocate(1024*1024);
		
		for (int i = 0; i < 10; i++) {
			int chunkSize = i*16;
			ByteBuffer sourceBuffer = ByteBuffer.allocate(chunkSize);
			new Random().nextBytes(sourceBuffer.array());
			Assert.assertEquals(chunkSize, this.cipher.update(sourceBuffer, targetBuffer));
			targetBuffer.flip();
			Assert.assertEquals(chunkSize, targetBuffer.remaining());
			targetBuffer.clear();
		}

		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		ByteBuffer paddingBuffer = ByteBuffer.allocate(16).put(targetBuffer);
		targetBuffer.clear();
		
		paddingBuffer.flip();
		
		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		Assert.assertEquals(paddingBuffer, targetBuffer);
		targetBuffer.clear();
	}
	
	@Test
	public void dynamicIncreasingChunking() throws Exception {
		ByteBuffer targetBuffer = ByteBuffer.allocate(1024*1024);
		int bytes = 0;
		
		for (int i = 0; i < 16; i++) {
			int chunkSize = i;
			bytes += i;
			
			ByteBuffer sourceBuffer = ByteBuffer.allocate(chunkSize);
			new Random().nextBytes(sourceBuffer.array());
			Assert.assertEquals(bytes / 16 * 16, this.cipher.update(sourceBuffer, targetBuffer));
			targetBuffer.flip();
			Assert.assertEquals(bytes / 16 * 16, targetBuffer.remaining());
			targetBuffer.clear();
			
			bytes = bytes % 16;
		}

		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		ByteBuffer paddingBuffer = ByteBuffer.allocate(16).put(targetBuffer);
		targetBuffer.clear();
		
		paddingBuffer.flip();
		
		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		Assert.assertNotEquals(paddingBuffer, targetBuffer);
		targetBuffer.clear();
	}
	
	@Test
	public void dynamicDecreasingChunking() throws Exception {
		ByteBuffer targetBuffer = ByteBuffer.allocate(1024*1024);
		int bytes = 0;
		
		for (int i = 0; i < 16; i++) {
			int chunkSize = 15-i;
			bytes += 15-i;
			
			ByteBuffer sourceBuffer = ByteBuffer.allocate(chunkSize);
			new Random().nextBytes(sourceBuffer.array());
			Assert.assertEquals(bytes / 16 * 16, this.cipher.update(sourceBuffer, targetBuffer));
			targetBuffer.flip();
			Assert.assertEquals(bytes / 16 * 16, targetBuffer.remaining());
			targetBuffer.clear();
			
			bytes = bytes % 16;
		}

		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		ByteBuffer paddingBuffer = ByteBuffer.allocate(16).put(targetBuffer);
		targetBuffer.clear();
		
		paddingBuffer.flip();
		
		Assert.assertEquals(16, this.cipher.doFinal(ByteBuffer.allocate(0), targetBuffer));
		targetBuffer.flip();
		Assert.assertEquals(16, targetBuffer.remaining());
		Assert.assertNotEquals(paddingBuffer, targetBuffer);
		targetBuffer.clear();
	}
	
	@Test
	public void lowTarget() throws Exception {
		ByteBuffer targetBuffer = ByteBuffer.allocate(5);
		ByteBuffer leftoverBuffer = ByteBuffer.allocate(32);
		
		System.out.println(this.cipher.update(ByteBuffer.allocate(0), targetBuffer));
		System.out.println(this.cipher.update(ByteBuffer.allocate(16), leftoverBuffer));
	}

}
