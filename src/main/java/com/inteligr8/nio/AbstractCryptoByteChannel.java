package com.inteligr8.nio;

import java.io.IOException;
import java.nio.channels.Channel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The base class for an encrypting or decrypting Java NIO ByteChannel.
 * 
 * @author brian@inteligr8.com
 */
abstract class AbstractCryptoByteChannel implements Channel {
    
	private final Logger logger = LoggerFactory.getLogger(AbstractCryptoByteChannel.class);
    
    private final CipherBuffer cipher;
    private final Channel channel;
    
    private boolean closed = false;
    
    public AbstractCryptoByteChannel(Channel channel, CipherParameters cparams)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
    	this.cipher = new CipherBuffer(cparams);
    	this.channel = channel;
    }
    
    /**
     * An accessor for the underlying NIO cipher buffer.
     * 
     * @return A cipher buffer
     */
    protected CipherBuffer getCipher() {
        return this.cipher;
    }
    
    /**
     * The number of bytes read as input for this channel.  This includes all
     * bytes, including any embedded artifacts like padding.  Any data buffered
     * by this channel is considered read, but not written.
     * 
     * @return A number of bytes; 0 if not yet read; never negative
     * @see CipherBuffer#getTotalBytesRead()
     */
    public long getTotalBytesRead() {
        return this.cipher.getTotalBytesRead();
    }
    
    /**
     * The number of bytes written as output for this channel.  This includes
     * bytes, including any embedded artifacts like padding.  Any data buffered
     * by this channel is considered read, but not written.
     * 
     * @return A number of bytes; 0 if not yet read; never negative
     * @see CipherBuffer#getTotalBytesWritten()
     */
    public long getTotalBytesWritten() {
        return this.cipher.getTotalBytesWritten();
    }

    /**
     * This channel is open.  If the underlying channel is closed, this channel
     * is considered not open.
     * 
     * @return true if open; false if closed; default state is open
     */
    @Override
    public boolean isOpen() {
    	return this.channel.isOpen() && !this.closed;
    }
    
    /**
     * This method marks this channel as closed.  It does not close the
     * underlying channel.  This allows for mid-channel crypto use cases.
     */
    @Override
    public void close() throws IOException {
        if (!this.closed && Status.Started.equals(this.getCipher().getStatus()))
            this.logger.warn("A channel was closed before it was properly finished");
        this.closed = true;
    }

}
