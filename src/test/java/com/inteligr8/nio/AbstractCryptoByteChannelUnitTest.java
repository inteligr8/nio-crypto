package com.inteligr8.nio;

import java.nio.charset.Charset;
import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

abstract class AbstractCryptoByteChannelUnitTest {
    
    private final Charset charset = Charset.forName("utf-8");
    
    private static Key key;
    private static int keySizeInBytes;
    
    @BeforeClass
    public static void createKey() throws DecoderException {
        byte[] keyInBytes = Hex.decodeHex("0123456789abcdef0123456789abcdef".toCharArray());
        key = new SecretKeySpec(keyInBytes, "AES");
        keySizeInBytes = keyInBytes.length;
    }
    
    public Charset getCharset() {
        return this.charset;
    }
    
    protected Key getKey() {
        return key;
    }
    
    protected int getKeySizeInBytes() {
        return keySizeInBytes;
    }
    
    @Test
    public void empty() throws Exception {
        this.test("", "0efb6bfed93b4d1ea2123ba4db075ff6");
    }
    
    @Test
    public void lessThan8b() throws Exception {
        this.test("Hello", "6aa9f9479de4e65399463357ada98066");
    }
    
    @Test
    public void exactly8b() throws Exception {
        this.test("Hi there", "be52f747f1a8608a35240f847a2b36ae");
    }
    
    @Test
    public void between8b16b() throws Exception {
        this.test("Hello World!", "9747ea180837d5caccc7a7ad94ab9f32");
    }
    
    @Test
    public void exactly16b() throws Exception {
        this.test("Hello over there", "1190f9879f9d173b3544d18db65d1e8270ea9a2d6f918a1a808849277d612e79");
    }
    
    @Test
    public void between16b32b() throws Exception {
        this.test("Howdy y'all compadres!", "5fd28457d8c440fe350c1f1ad5038522a426ec00eb40246a670367a5c966d570");
    }
    
    @Test
    public void moreThan32b() throws Exception {
        this.test("Hello to all and to all a good night", "3383ee4d7b65c32124b992da2e888dec1635ad33a399336d7763f040bad669036baf3253c9859029d13c91c3e9d8a463");
    }
    
    public abstract void test(String text, String hex) throws Exception;

}
