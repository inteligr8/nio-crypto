/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the JCE Cipher using the Java NIO library and
 * concepts.  The JCE Cipher accepts ByteBuffer, but it can be complicated to
 * use.  This class makes encryption and decryption act more like typical NIO
 * buffer operations.
 * 
 * @author brian@inteligr8.com
 * @see DigestBuffer
 * @see DecryptingReadableByteChannel
 * @see EncryptingReadableByteChannel
 * @see DecryptingWritableByteChannel
 * @see EncryptingWritableByteChannel
 */
@PublicApi
public class CipherBuffer extends AbstractBuffer {
	
	private final Logger logger = LoggerFactory.getLogger(CipherBuffer.class);
    private final String DEFAULT_CIPHER_MODE = "ECB";
    private final String DEFAULT_CIPHER_PADDING = "PKCS5Padding";
    private final Pattern ALGORITHM_PATTERN = Pattern.compile("(.+)/(.+)/(.+)");
	
    private final int cipherMode;
	private final Key key;
	private final Cipher cipher;
	private final ByteBuffer leftoverBuffer;
	
	public CipherBuffer(CipherParameters cparams)
	throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		this.cipherMode = cparams.getCipherMode();
        this.key = cparams.getKey();
        
        String transformation = cparams.getTransformation();
        if (transformation == null)
        	transformation = this.key.getAlgorithm();
        
        Cipher cipher = cparams.getProvider() == null ? Cipher.getInstance(transformation) : Cipher.getInstance(transformation, cparams.getProvider());
        if (this.logger.isDebugEnabled())
        	this.logger.debug("cipher transformation: " + cipher.getAlgorithm());

        // if the algorithm requires a stream cipher
        if (cipher.getBlockSize() == 0) {
            cipher.init(this.cipherMode, this.key);
            this.leftoverBuffer = ByteBuffer.allocate(cipher.getOutputSize(0)*2);
        // otherwise algorithm requires a block cipher
        } else {
            // if a mode not specified, force a mode
            Matcher matcher = ALGORITHM_PATTERN.matcher(cipher.getAlgorithm());
            if (!matcher.matches()) {
                if (cipher.getAlgorithm().contains("/"))
                    throw new IllegalArgumentException("The transformation contains a slash, but should contain 2 slashes, separating the algorithm, mode, and padding: " + cipher.getAlgorithm());
                transformation = transformation + "/" + DEFAULT_CIPHER_MODE + "/" + DEFAULT_CIPHER_PADDING;
                if (this.logger.isDebugEnabled())
                	this.logger.debug("defaulting to transformation: " + cipher.getAlgorithm());
                
                matcher = ALGORITHM_PATTERN.matcher(transformation);
                if (!matcher.matches())
                    throw new IllegalArgumentException("The algorithm is not formatted properly: " + transformation);
                
                cipher = cparams.getProvider() == null ? Cipher.getInstance(transformation) : Cipher.getInstance(transformation, cparams.getProvider());
            }
            
            this.initialized = true;
            
            if (cparams.getInitializationVector() != null) {
                if (this.logger.isDebugEnabled())
                	this.logger.debug("initializing cipher with specified IV scrambler");
                cipher.init(this.cipherMode, this.key, new IvParameterSpec(cparams.getInitializationVector()));
            } else if (cparams.isGenerateInitializationVector()) {
                if (this.logger.isDebugEnabled())
                	this.logger.debug("initializing cipher with random IV scrambler");
                byte[] iv = new byte[cipher.getBlockSize()];
                new SecureRandom().nextBytes(iv);
                cipher.init(this.cipherMode, this.key, new IvParameterSpec(iv));
            } else if (cparams.isDeferInitializationVector()) {
                if (this.logger.isDebugEnabled())
                	this.logger.debug("delaying cipher initialization ...");
        		this.initialized = false;
            } else {
                if (this.logger.isDebugEnabled())
                	this.logger.debug("initializing cipher without IV scrambler");
                cipher.init(this.cipherMode, this.key);
            }
            
            this.leftoverBuffer = ByteBuffer.allocate(cipher.getBlockSize()*2);
        }

        this.leftoverBuffer.flip();
        this.cipher = cipher;
	}
	
	/**
	 * An accessor for the JCA key.
	 * 
	 * @return A JCA key
	 */
	public Key getKey() {
		return this.key;
	}
	
	/**
	 * An accessor for the underlying JCE cipher object.
	 * 
	 * @return A JCE cipher
	 */
	public Cipher getCipher() {
		return this.cipher;
	}
    
	/**
	 * An accessor for the optional initialization vector.
	 * 
	 * https://en.wikipedia.org/wiki/Initialization_vector
	 * 
	 * @return A byte array of the IV with a length of the key or block size; or null
	 */
    public byte[] getInitializationVector() {
        return this.cipher.getIV();
    }
    
    /**
     * A modifier for the optional initialization vector.
     * 
     * https://en.wikipedia.org/wiki/Initialization_vector
     * 
     * @param initializationVector A byte array of the IV with a length of the key or block size; or null to unset
     * @throws InvalidAlgorithmParameterException The specified algorithm does not support an IV or the specified length is incorrect
     */
    public void setInitializationVector(byte[] initializationVector) throws InvalidAlgorithmParameterException {
        try {
            if (this.logger.isDebugEnabled())
            	this.logger.debug("re-initializing cipher with specified IV scrambler");
            this.cipher.init(this.cipherMode, this.key, new IvParameterSpec(initializationVector));
            this.initialized = true;
        } catch (InvalidKeyException ike) {
            throw new RuntimeException("This will never happen; the key was already declared valid by previous init()");
        }
    }
	
    /**
     * This method flushes any cipher cache, padding, and leftovers into the
     * specified buffer.  Any subsequent call to `stream()` will fail.  If the
     * specified buffer is too small, you will need to call this method
     * multiple times to complete the flush.  You should expect to call it
     * until it returns `false`.
     * 
     * @param targetBuffer A NIO buffer ready for writing
     * @return true if more data may need flushed; false if subsequent calls are not expected
     * @throws BadPaddingException For decryption; an end of stream padding issue occurred; maybe mismatched padding configurations
     * @throws IllegalBlockSizeException For encryption; the stream likely requires padding
     * @see CipherBuffer#stream(ByteBuffer, ByteBuffer)
     */
	public boolean flush(ByteBuffer targetBuffer) throws BadPaddingException, IllegalBlockSizeException {
        if (this.logger.isTraceEnabled())
            this.logger.trace("flush(" + targetBuffer.remaining() + ")");
        
        long oldTargetBytesWritten = this.targetBytesWritten;
        
        // leftovers exist; must eat
        if (this.leftoverBuffer.remaining() > 0) {
            if (this.logger.isTraceEnabled())
                this.logger.trace("flush(" + targetBuffer.remaining() + "): draining leftovers");
            
            this.targetBytesWritten += this.copyBuffer(this.leftoverBuffer, targetBuffer);
            
            if (this.leftoverBuffer.remaining() > 0)
            	return true;
        // otherwise no more data anywhere
        } else {
            if (this.logger.isTraceEnabled())
                this.logger.trace("flush(" + targetBuffer.remaining() + "): nothing to flush");
        }
		
		if (!this.finished) {
			// ready for writing
			this.leftoverBuffer.clear();
			try {
	        	this.cipher.doFinal(ByteBuffer.allocate(0), this.leftoverBuffer);
        		this.finished = true;
			} catch (ShortBufferException sbe) {
				// this should never happen
				throw new BufferOverflowException();
			}
			
			// ready for reading
			this.leftoverBuffer.flip();
			
			this.targetBytesWritten += this.copyBuffer(this.leftoverBuffer, targetBuffer);
			
			if (this.leftoverBuffer.hasRemaining())
				return true;
		}
		
		return oldTargetBytesWritten < this.targetBytesWritten;
    }
	
	/**
	 * This method streams crypto from a source NIO buffer to a target NIO
	 * buffer.  This differs from the JRE implementation as it is flexible on
	 * the size of the target buffer.  You should expect to call this method
     * until it returns `false`.  Afterwards, you will start the `flush`
     * phase.
	 * 
	 * @param sourceBuffer A NIO buffer ready for reading
	 * @param targetBuffer A NIO buffer ready for writing
	 * @return true if the source still needs streamed; false if subsequent calls should instead be flushed
	 * @see CipherBuffer#flush(ByteBuffer)
	 */
	public boolean stream(ByteBuffer sourceBuffer, ByteBuffer targetBuffer) {
		if (sourceBuffer == null)
			throw new IllegalArgumentException();
		if (targetBuffer == null)
			throw new IllegalArgumentException();
		if (sourceBuffer != null && this.finished)
			throw new IllegalStateException("There should no longer be a source to this cipher");
		
        if (this.logger.isTraceEnabled())
            this.logger.trace("stream(" + sourceBuffer.remaining() + ", " + targetBuffer.remaining() + ")");

        if (!this.started)
            this.started = true;
        
        long oldTargetBytesWritten = this.targetBytesWritten;
        
        // if there are leftovers, start there
        if (this.leftoverBuffer.remaining() > 0) {
            if (this.logger.isTraceEnabled())
                this.logger.trace("stream(" + sourceBuffer.remaining() + ", " + targetBuffer.remaining() + "): draining leftovers");
            
            this.targetBytesWritten += this.copyBuffer(this.leftoverBuffer, targetBuffer);
            
            // if we filled the buffer, then we are done for now
            if (targetBuffer.remaining() == 0) {
                if (this.logger.isTraceEnabled())
                    this.logger.trace("stream(" + sourceBuffer.remaining() + ", " + targetBuffer.remaining() + "): target filled with leftovers");
                
            	// all buffer work; nothing was read from the source buffer, even if some bytes were ready
                return sourceBuffer.hasRemaining();
            }

            if (this.leftoverBuffer.remaining() > 0)
                throw new AssertionError("This should never happen");
            this.leftoverBuffer.clear();
            this.leftoverBuffer.flip();
        }
        
        int blockSize = this.cipher.getBlockSize();
        if (blockSize <= 0)
        	blockSize = this.cipher.getOutputSize(0);
        
        int limitBackup = sourceBuffer.limit();

        // restrict the source so that it doesn't result in more data than the target can take
        int requiredOutputBufferSize = this.cipher.getOutputSize(sourceBuffer.remaining());
    	// back off block by block
    	// TODO could be optimized in case the source buffer is WAY bigger than the target buffer
        while (requiredOutputBufferSize > targetBuffer.remaining() && sourceBuffer.remaining() >= blockSize) {
        	sourceBuffer.limit(sourceBuffer.limit() - blockSize);
        	requiredOutputBufferSize = this.cipher.getOutputSize(sourceBuffer.remaining());
        }
        
        try {
	        // if the target is big enough, perform the crypto copy
	        if (requiredOutputBufferSize <= targetBuffer.remaining()) {
	        	this.sourceBytesRead += sourceBuffer.remaining();
	        	this.targetBytesWritten += this.cipher.update(sourceBuffer, targetBuffer);
	        } else {
	        	// target buffer is really small; hopefully bigger on next call
	        }
	        
	        // if the source was restricted because the target wasn't big enough
	        if (limitBackup > sourceBuffer.limit()) {
	        	// read up to an extra block
	        	sourceBuffer.limit(Math.min(sourceBuffer.limit() + blockSize, limitBackup));
	        }
	        
        	// ready for writing
        	this.leftoverBuffer.compact();
        	
        	this.sourceBytesRead += sourceBuffer.remaining();
        	this.targetBytesWritten += this.cipher.update(sourceBuffer, this.leftoverBuffer);

        	// ready for reading
            this.leftoverBuffer.flip();
            
            // using the extra, fill the target (applicable when its remainder is less than a block)
            this.copyBuffer(this.leftoverBuffer, targetBuffer);
        } catch (ShortBufferException sbe) {
        	// this should never happen, as precautions have been put in place
        	throw new BufferOverflowException();
        }

		sourceBuffer.limit(limitBackup);
		return oldTargetBytesWritten < this.targetBytesWritten || sourceBuffer.hasRemaining();
	}
	
}
