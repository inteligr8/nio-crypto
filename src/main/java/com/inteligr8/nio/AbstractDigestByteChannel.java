/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.channels.Channel;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The base class for a hashing Java NIO ByteChannel.
 * 
 * @author brian@inteligr8.com
 */
abstract class AbstractDigestByteChannel implements Channel {
    
	private final Logger logger = LoggerFactory.getLogger(AbstractDigestByteChannel.class);
    
    private final DigestBuffer digest;
    private final Channel channel;
    
    private boolean closed = false;
    
    public AbstractDigestByteChannel(Channel channel, DigestParameters dparams) throws NoSuchAlgorithmException {
    	this.digest = new DigestBuffer(dparams);
    	this.channel = channel;
    }
    
    protected DigestBuffer getDigest() {
        return this.digest;
    }
    
    public int getHashSize() {
    	return this.digest.getMessageDigest().getDigestLength();
    }
    
    /**
     * The number of bytes read as input for this channel.
     * 
     * @return A number of bytes; 0 if not yet read; never negative
     */
    public long getTotalBytesRead() {
        return this.digest.getTotalBytesRead();
    }
    
    /**
     * The number of bytes written as output for this channel.
     * 
     * @return A number of bytes; 0 if not yet read; never negative
     */
    public long getTotalBytesWritten() {
        return this.digest.getTotalBytesWritten();
    }

    /**
     * This channel is open.  If the underlying channel is closed, this channel
     * is considered not open.
     * 
     * @return true if open; false if closed; default state is open
     */
    @Override
    public boolean isOpen() {
    	return this.channel.isOpen() && !this.closed;
    }
    
    /**
     * This method marks this channel as closed.  It does not close the
     * underlying channel.  This allows for mid-channel hashing use cases.
     */
    @Override
    public void close() throws IOException {
        if (!this.closed && Status.Started.equals(this.getDigest().getStatus()))
            this.logger.warn("A channel was closed before it was properly finished");
        this.closed = true;
    }

}
