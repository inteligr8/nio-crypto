/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.Flushable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.security.DigestException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

/**
 * This class implements a NIO writable byte channel that hashes the contents
 * buffers as they are written to the underlying writable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see HashingReadableByteChannel
 */
@PublicApi
public class HashingWritableByteChannel extends AbstractDigestByteChannel implements WritableByteChannel, Flushable {
	
	protected final WritableByteChannel wbchannel;
	
	public HashingWritableByteChannel(WritableByteChannel wbchannel, DigestParameters dparams) throws NoSuchAlgorithmException {
		super(wbchannel, dparams);
		this.wbchannel = wbchannel;
	}
    
	/**
	 * @see AbstractBuffer#getTotalBytesRead()
	 */
    public long getRawBytesRead() {
        return this.getTotalBytesRead();
    }

	/**
	 * @see AbstractBuffer#getTotalBytesWritten()
	 */
    public long getHashBytesWritten() {
        return this.getTotalBytesWritten();
    }
    
    /**
     * This method hashes the contents of the specified buffer until it is
     * empty.  The hash will not be written to the underlying writable byte
     * channel until the `flush` phase.
     * 
     * @param buffer A NIO buffer ready for reading
     * @return Always 0
     * @throws IOException An I/O or hashing exception occurred
     */
    @Override
    public int write(ByteBuffer buffer) throws IOException {
		try {
			return this._write(buffer);
		} catch (DigestException de) {
			throw new IOException("This should never happen", de);
		}
    }
    
    /**
     * This method writes the cached hash to the underlying writable byte
     * channel.
     * 
     * @throws IOException An I/O or hashing exception occurred
     */
    @Override
    public void flush() throws IOException {
		try {
			this._flush();
		} catch (DigestException de) {
			throw new IOException("This should never happen", de);
		}
    }
    
    /**
     * This method flushes and closes this channel.
     * 
     * @throws IOException An I/O or crypto exception occurred
     */
    @Override
    public void close() throws IOException {
    	this.flush();
    	super.close();
    }
    
    /**
     * This method hashes and writes to the underlying channel until the
     * specified buffer is empty.
     * 
     * @param buffer A NIO buffer ready for reading
     * @return The number of bytes written to the underlying channel; never negative
     * @throws IOException An I/O or hashing exception occurred
     * @throws DigestException A hashing issue occurred
     */
    protected int _write(ByteBuffer buffer) throws IOException, DigestException {
    	if (!this.isOpen())
    		return 0;
    	
    	this.getDigest().stream(buffer);
    	return 0;
    }
    
    /**
     * This method encrypts or decrypts and writes to the underlying channel
     * until the internal caches and buffers are empty.
     * 
     * @return The number of bytes written to the underlying channel; never negative
     * @throws IOException An I/O or crypto exception occurred
     * @throws DigestException A hashing issue occurred
     */
    protected int _flush() throws IOException, DigestException {
    	if (!this.isOpen())
    		return 0;
    	
    	int totalBytesWritten = 0;
    	
    	// ready for writing
    	ByteBuffer writeBuffer = ByteBuffer.allocate(this.getDigest().getMessageDigest().getDigestLength()); 
		
    	while (this.getDigest().flush(writeBuffer))
    		;
    	
        writeBuffer.flip();    // ready for reading
        totalBytesWritten += this.wbchannel.write(writeBuffer);
        writeBuffer.compact(); // ready for writing
    	
    	return totalBytesWritten;
    }

}
