package com.inteligr8.nio;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

abstract class AbstractCipherUnitTest {
	
	public abstract String getDefaultAlgorithm();
	public abstract int getDefaultKeySize();

	public String getDefaultTransformation() {
		return this.getDefaultAlgorithm();
	}
	
	public int getDefaultKeySizeInBytes() {
		return this.getDefaultKeySize() / 8;
	}
	
	protected SecretKey generateSecretKey() throws NoSuchAlgorithmException {
		return this.generateSecretKey(this.getDefaultAlgorithm(), this.getDefaultKeySize());
	}
	
	protected SecretKey generateSecretKey(String algorithm, int keysize) throws NoSuchAlgorithmException {
		KeyGenerator keygen = KeyGenerator.getInstance(algorithm);
		keygen.init(keysize);
		return keygen.generateKey();
	}

	protected KeyPair generatePublicPrivateKey() throws NoSuchAlgorithmException {
		return this.generatePublicPrivateKey(this.getDefaultAlgorithm(), this.getDefaultKeySize());
	}

	protected KeyPair generatePublicPrivateKey(String algorithm, int keysize) throws NoSuchAlgorithmException {
		KeyPairGenerator keygen = KeyPairGenerator.getInstance(algorithm);
		keygen.initialize(keysize);
		return keygen.generateKeyPair();
	}
	
	protected CipherBuffer createCipher(int cipherMode, Key key) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key));
	}
	
	protected CipherBuffer createCipher(int cipherMode, Key key, String transformation) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key, transformation));
	}
	
	protected CipherBuffer createCipher(int cipherMode, Key key, CipherBuffer cipher) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key));
	}
	
	protected CipherBuffer createCipher(int cipherMode, Key key, String transformation, CipherBuffer cipher) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key, transformation));
	}

}
