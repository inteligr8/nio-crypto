package com.inteligr8.nio;

import java.nio.ByteBuffer;

/**
 * This class serves as the base for custom multi-part buffer implementations.
 * 
 * @author brian@inteligr8.com
 */
abstract class AbstractBuffer {
	
	protected long sourceBytesRead = 0L;
	protected long targetBytesWritten = 0L;
	protected boolean started = false;
	protected boolean finished = false;
	protected boolean initialized = false;
	
	public Status getStatus() {
		if (this.finished) return Status.Finished;
		else if (this.started) return Status.Started;
		else if (this.initialized) return Status.Initialized;
		else return Status.Uninitialized;
	}
	
	/**
	 * An accessor for the total number of bytes read from supplied source
	 * buffers.
	 * 
	 * The total number of bytes read only indirectly affects the total number
	 * of bytes written.  That impact is dictated by the context of the buffer
	 * implementation.  If there is caching, writing will appear behind.  If
	 * there is padding, writing may appear ahead.  It is best to never assume
	 * any predictable relationship between one and the other.
	 * 
	 * @return A number of bytes; never negative
	 * @see AbstractBuffer#getTotalBytesWritten()
	 */
	public long getTotalBytesRead() {
		return this.sourceBytesRead;
	}
	
	/**
	 * An accessor for the total number of bytes written to supplied target
	 * buffers.
	 * 
	 * The total number of bytes written is only indirectly dependent on the
	 * total number of bytes read.  That dependency is dictated by the context
	 * of the buffer implementation.  If there is caching, writing will appear
	 * behind.  If there is padding, writing may appear ahead.  It is best to
	 * never assume any predictable relationship between one and the other.
	 * 
	 * @return A number of bytes; never negative
	 * @see AbstractBuffer#getTotalBytesRead()
	 */
	public long getTotalBytesWritten() {
		return this.targetBytesWritten;
	}
    
    /**
     * This method copies every byte possible from one buffer to another.  It
     * stops copying gracefully when either the source buffer is empty or the
     * target buffer is full.
     * 
     * @param sourceBuffer A NIO buffer ready for reading
     * @param targetBuffer A NIO buffer ready for writing
     * @return The number of bytes transferred; never negative; could be 0
     */
    protected int copyBuffer(ByteBuffer sourceBuffer, ByteBuffer targetBuffer) {
        int bytesToCopy = Math.min(sourceBuffer.remaining(), targetBuffer.remaining());
        int realLimit = sourceBuffer.limit();
        int tmpLimit = sourceBuffer.position() + bytesToCopy;
        
        // read only the amount both buffers can handle
        sourceBuffer.limit(tmpLimit);
        
        // do the actual transfer of bytes
        targetBuffer.put(sourceBuffer);
        
        // reset the limit to its actual value (which could be the same)
        sourceBuffer.limit(realLimit);
        
        return bytesToCopy;
    }
	
}
