package com.inteligr8.nio;

import java.security.Key;

import com.inteligr8.nio.CipherParameters.IVSource;

public class IVSymmetricBlockCipherUnitTest extends SymmetricBlockCipherUnitTest {
	
	@Override
	public String getDefaultTransformation() {
		return "AES/CBC/PKCS5Padding";
	}
	
	@Override
	public CipherBuffer createCipher(int cipherMode, Key key, String transformation) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key, transformation, IVSource.Generated));
	}
	
	public CipherBuffer createCipher(int cipherMode, Key key, String transformation, CipherBuffer cipher) throws Exception {
		return new CipherBuffer(new CipherParameters(cipherMode, key, transformation, cipher.getInitializationVector()));
	}

}
