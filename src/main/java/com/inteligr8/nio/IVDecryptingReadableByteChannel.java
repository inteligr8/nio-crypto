/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;

import javax.crypto.NoSuchPaddingException;

/**
 * This class assumes a plain text initialization vector before the start of
 * the encrypted content.  Otherwise it acts identical to the
 * decrypting readable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see DecryptingReadableByteChannel
 */
@PublicApi
public class IVDecryptingReadableByteChannel implements ReadableByteChannel, DecryptionMetered {
    
    private final ReadableByteChannel rbchannel;
    private final DecryptingReadableByteChannel drbchannel;
    private boolean readIV = false;
    
    /**
     * @param rbchannel A readable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVDecryptingReadableByteChannel(ReadableByteChannel rbchannel, Key key, String transformation)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        this.rbchannel = rbchannel;
        this.drbchannel = new DecryptingReadableByteChannel(rbchannel, new DecryptingCipherParameters(key, transformation, true));
    }

    /**
     * @param rbchannel A readable byte channel
     * @param key A JCA key
     * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
     * @param provider A JCA provider
     * @throws NoSuchAlgorithmException The algorithm is not supported by the JCA provider
     * @throws NoSuchPaddingException The padding is not supported by the JCA provider and cipher mode
     * @throws InvalidKeyException The key is not compatible with the specified transformation
     * @throws InvalidAlgorithmParameterException The algorithm does not support IV or requires an IV
     */
    public IVDecryptingReadableByteChannel(ReadableByteChannel rbchannel, Key key, String transformation, Provider provider)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        this.rbchannel = rbchannel;
        this.drbchannel = new DecryptingReadableByteChannel(rbchannel, new DecryptingCipherParameters(key, transformation, true, provider));
    }
    
    /**
     * @see DecryptingReadableByteChannel#close()
     */
    @Override
    public void close() throws IOException {
        this.drbchannel.close();
    }

    /**
     * @see DecryptingReadableByteChannel#isOpen()
     */
    @Override
    public boolean isOpen() {
        return this.drbchannel.isOpen();
    }

    /**
     * @see DecryptionMetered#getEncryptedBytesRead()
     */
    @Override
    public long getEncryptedBytesRead() {
    	long bytes = this.drbchannel.getEncryptedBytesRead();
    	if (this.readIV)
    		bytes += this.drbchannel.getCipher().getInitializationVector().length;
    	return bytes;
    }

    /**
     * @see DecryptionMetered#getDecryptedBytesWritten()
     */    
    @Override
    public long getDecryptedBytesWritten() {
        return this.drbchannel.getDecryptedBytesWritten();
    }
    
    /**
     * This method will read the IV from the raw readable byte buffer,
     * configure the cipher to use that IV, and then decrypt the rest of the
     * readable byte buffer.
     * 
     * @param buffer A NIO buffer ready for writing
     * @return The number of bytes read from the underlying channel; -1 for EOF; 0 is possible and should be re-read
     * @see DecryptingReadableByteChannel#read(ByteBuffer)
     */
    @Override
    public int read(ByteBuffer buffer) throws IOException {
        int bytes = 0;
        if (!this.readIV) {
        	// read the IV
            ByteBuffer ivbuffer = ByteBuffer.allocate(this.drbchannel.getCipher().getCipher().getBlockSize());
            bytes += this.rbchannel.read(ivbuffer);
            ivbuffer.flip(); // ready for reading
            
            byte[] iv = new byte[ivbuffer.remaining()];
            ivbuffer.get(iv);
            try {
                this.drbchannel.getCipher().setInitializationVector(iv);
            } catch (InvalidAlgorithmParameterException iape) {
                throw new IOException(iape.getMessage(), iape);
            }
            
            this.readIV = true;
        }
        
        bytes += this.drbchannel.read(buffer);
        return bytes;
    }

}
