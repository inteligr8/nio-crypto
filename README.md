# Java New I/O Crypto Library

The official distributions of [Java](https://openjdk.java.net/) include cryptographic functions in a framework called the [Java Cryptography Extension (JCE)](https://en.wikipedia.org/wiki/Java_Cryptography_Extension).  JCE provides the API layer and corresponding implementations of cryptographic functions like key generation, key storage/retrieval, and cipher encryption/decryption.  The JCE API supports those basic functions along with Java I/O based capabilities using `InputStream` and `OutputStream` implementations.

JCE is part of the [Java Cryptography Architecture (JCA)](https://en.wikipedia.org/wiki/Java_Cryptography_Architecture), which has been around even longer.  It is also in scope of this library.  In addition to the JCE cryptographic functions, JCA includes hashing functions.  It has the same reliance on Java I/O based capabilities.

Since the introduction of JCA/JCE, Java has introduced the ["New I/O" (NIO)](https://en.wikipedia.org/wiki/Non-blocking_I/O_%28Java%29) framework as a complement to the Java (blocking) I/O (BIO) framework.  NIO originally meant "Non-blocking I/O", but it also provides blocking as well, so "New I/O" is a better representation.

Non-blocking I/O has huge advantages in performance over blocking I/O for many applications.  For instance, it is recommended that Apache Tomcat configurations use NIO connectors for clients due to its performance advantages.  Since NIO also supports blocking mode, it effectively depreciated the legacy I/O (but not officially).

## Using

### Including

To use this library, you must include it as a dependency to your project.  An example configuration for Apache Maven is provided below.  Either set the `inteligr8.nio-crypto.version` as a property or replace it with a valid version.
```xml
<project ..>
	...
	<dependencies>
		...
		<dependency>
			<groupId>com.inteligr8</groupId>
			<artifactId>nio-crypto</artifactId>
			<version>${inteligr8.nio-crypto.version}</version>
		</dependency>
		...
	</dependencies>
	...
	<repositories>
		...
		<repository>
			<id>inteligr8-releases</id>
			<url>http://repos.inteligr8.com/nexus/repository/inteligr8-public</url>
		</repository>
		...
	</repositories>
</project>
```

### Developing

There are many different algorithms that are available for encrypting, decrypting, and hashing content.  This project does not care about those details.  It does not care about key generation, storage, or retrieval.  It only cares about the streaming of content through a cipher or digest.  The most primal I/O operations.  There is a default set of algorithms come from the JVM default JCA/JCE providers.  You can include other JCA/JCE providers to expand the supported algorithms, cipher modes, and padding options.  Some of those providers interface directly with hardware or the network for enhanced security or performance.

A [cipher](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/javax/crypto/Cipher.html) is defined by an algorithm, cipher mode, and padding.  These are married together into a single parameter in JCE called a *transformation*.  That same parameter is used in this library.  You will also need an appropriate key and your content.  In Java 8 and earlier, you will need another provider or an extension to the default provider to support high stength crypto.  You can [download the Oracle Java extension here](https://www.oracle.com/java/technologies/javase-jce-all-downloads.html).

A [digest](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/security/MessageDigest.html) is defined by a just an algorithm name.

You can find a list of supported cipher transformations and digest algorithms in the official Java documentation linked in the paragraphs above.

### Resources

* [Sample code for common algorithms](/inteligr8/nio-crypto/src/stable/src/test/java/com/inteligr8/nio/CommonSamples.java)
* [JavaDoc](http://api.inteligr8.com/apidocs/nio-crypto-javadoc-jar)
