/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.DigestException;
import java.security.NoSuchAlgorithmException;

/**
 * This class implements a NIO readable byte channel that hashes the contents
 * of the underlying readable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see HashingWritableByteChannel
 */
@PublicApi
public class HashingReadableByteChannel extends AbstractDigestByteChannel implements ReadableByteChannel {
	
	protected final ReadableByteChannel rbchannel;
	
	private int chunkSize;
	protected ByteBuffer readBuffer;
	
	public HashingReadableByteChannel(ReadableByteChannel rbchannel, DigestParameters dparams) throws NoSuchAlgorithmException {
		super(rbchannel, dparams);
		this.rbchannel = rbchannel;
		this.setChunkSize(16384);
	}
	
	/**
	 * This method overrides the default chunk size for reading this channel
	 * into buffers.  The lower the value, the less memory usage.  The higher
	 * the value, the higher performance.
	 * 
	 * @param chunkSize A number of bytes; defaults to 16KB
	 */
	public void setChunkSize(int chunkSize) {
		if (this.readBuffer != null && this.readBuffer.hasRemaining())
			throw new IllegalStateException("You cannot change the buffer size while the buffer is actively used");
		this.chunkSize = chunkSize;
		this.readBuffer = ByteBuffer.allocate(this.chunkSize);
		this.readBuffer.flip(); // ready for reading by default
	}
    
	/**
	 * @see AbstractBuffer#getTotalBytesRead()
	 */
    public long getRawBytesRead() {
        return this.getTotalBytesRead();
    }
    
    /**
     * @see AbstractBuffer#getTotalBytesWritten()
     */
    public long getHashBytesWritten() {
        return this.getTotalBytesWritten();
    }
    
    /**
     * This method reads and hashes the underlying channel until it is empty.
     * It writes the hash to the specified buffer.
     * 
     * @param buffer A NIO buffer ready for writing
     * @return The number of bytes read from the underlying channel; -1 for EOF; 0 is possible and should be re-read
     * @throws IOException An I/O exception occurred
     */
    @Override
    public int read(ByteBuffer buffer) throws IOException {
		try {
			return this._read(buffer);
		} catch (DigestException de) {
			throw new IOException("This should never happen", de);
		}
    }
    
    /**
     * This method reads and hashes the underlying channel until it is empty.
     * It writes the hash to the specified buffer.
     * 
     * @param buffer A NIO buffer ready for writing
     * @return The number of bytes read from the underlying channel; -1 for EOF; 0 is possible and should be re-read
     * @throws IOException An I/O or hashing exception occurred
     */
    protected int _read(ByteBuffer buffer) throws IOException, DigestException {
    	int totalBytesRead = 0;
    	
    	this.readBuffer.compact(); // ready for writing
    	int bytesRead = this.rbchannel.read(this.readBuffer);
		this.readBuffer.flip();    // ready for reading
		
		if (!Status.Finished.equals(this.getDigest().getStatus())) {
			do {
				if (bytesRead > 0)
					totalBytesRead += bytesRead;
				
				this.getDigest().stream(this.readBuffer);
				
		    	this.readBuffer.compact(); // ready for writing
		    	bytesRead = this.rbchannel.read(this.readBuffer);
				this.readBuffer.flip();    // ready for reading
			} while (bytesRead > 0);
		}

    	if (bytesRead > 0)
    		totalBytesRead += bytesRead;
    	
    	if (totalBytesRead > 0)
    		// something read from underlying channel
    		return totalBytesRead;
    	
    	return this.getDigest().flush(buffer) ? 0 : -1;
    }

}
