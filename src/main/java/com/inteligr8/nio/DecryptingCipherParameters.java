/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.security.Key;
import java.security.Provider;

import javax.crypto.Cipher;

/**
 * This class provides a more specific set of constructors and restricted
 * modifiers for the purposes of delivering parameters to decryptiong classes
 * in this library.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public class DecryptingCipherParameters extends CipherParameters {
	
	/**
	 * @param key A JCA key
	 */
	public DecryptingCipherParameters(Key key) {
		super(Cipher.DECRYPT_MODE, key);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 */
	public DecryptingCipherParameters(Key key, String transformation) {
		super(Cipher.DECRYPT_MODE, key, transformation);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param deferInitializationVector The intent to set the IV after construction of this object
	 * @see CipherBuffer#setInitializationVector(byte[])
	 */
	public DecryptingCipherParameters(Key key, String transformation, boolean deferInitializationVector) {
		super(Cipher.DECRYPT_MODE, key, transformation, deferInitializationVector ? IVSource.Deferred : IVSource.None);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param initializationVector A byte array of the optional IV
	 */
	public DecryptingCipherParameters(Key key, String transformation, byte[] initializationVector) {
		super(Cipher.DECRYPT_MODE, key, transformation, initializationVector);
	}

	/**
	 * @param key A JCA key
	 * @param provider A JCA provider
	 */
	public DecryptingCipherParameters(Key key, Provider provider) {
		super(Cipher.DECRYPT_MODE, key, provider);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param provider A JCA provider
	 */
	public DecryptingCipherParameters(Key key, String transformation, Provider provider) {
		super(Cipher.DECRYPT_MODE, key, transformation, provider);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param deferInitializationVector The intent to set the IV after construction of this object
	 * @param provider A JCA provider
	 * @see CipherBuffer#setInitializationVector(byte[])
	 */
	public DecryptingCipherParameters(Key key, String transformation, boolean deferInitializationVector, Provider provider) {
		super(Cipher.DECRYPT_MODE, key, transformation, deferInitializationVector ? IVSource.Deferred : IVSource.None, provider);
	}

	/**
	 * @param key A JCA key
	 * @param transformation A JCE transformation in the pattern 'algorithm/cipher_mode/padding'
	 * @param initializationVector A byte array of the optional IV
	 * @param provider A JCA provider
	 */
	public DecryptingCipherParameters(Key key, String transformation, byte[] initializationVector, Provider provider) {
		super(Cipher.DECRYPT_MODE, key, transformation, initializationVector, provider);
	}
	
	@Override
	public final CipherParameters setCipherMode(int cipherMode) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public final DecryptingCipherParameters setGenerateInitializationVector(boolean generateInitializationVector) {
		throw new UnsupportedOperationException();
	}
	
}
