/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * This class implements a NIO readable byte channel that encrypts the buffers
 * as it is read from an underlying readable byte channel.
 * 
 * @author brian@inteligr8.com
 * @see EncryptingWritableByteChannel
 * @see DecryptingReadableByteChannel
 */
@PublicApi
public class EncryptingReadableByteChannel extends AbstractCryptoReadableByteChannel implements EncryptionMetered {
	
	public EncryptingReadableByteChannel(ReadableByteChannel rbchannel, EncryptingCipherParameters cparams)
	throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		super(rbchannel, cparams);
	}

	/**
	 * @see EncryptionMetered#getDecryptedBytesRead()
	 */
	@Override
    public long getDecryptedBytesRead() {
        return this.getTotalBytesRead();
    }

	/**
	 * @see EncryptionMetered#getEncryptedBytesWritten()
	 */
	@Override
    public long getEncryptedBytesWritten() {
        return this.getTotalBytesWritten();
    }

    /**
     * This method reads and encrypts the underlying channel until it is either
     * empty or the specified buffer is full.
     * 
     * @param buffer A NIO buffer ready for writing
     * @return The number of bytes read from the underlying channel; -1 for EOF; 0 is possible and should be re-read
     * @throws IOException An I/O or crypto exception occurred
     */
    @Override
    public int read(ByteBuffer buffer) throws IOException {
		try {
			return this._read(buffer);
		} catch (IllegalBlockSizeException ibse) {
			throw new IOException("An block size issue was discovered in the decrypted channel", ibse);
		} catch (BadPaddingException bpe) {
			// only for encryption
			throw new AssertionError("This should never happen", bpe);
		}
    }

}
