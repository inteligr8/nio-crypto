package com.inteligr8.nio;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.Key;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.crypto.KeyGenerator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StreamingCryptoByteChannelUnitTest {
    
	private static File m2dir;
    private static Key key;
    
    @BeforeClass
    public static void createKey() throws Exception {
    	String m2path = System.getProperty("user.m2path");
    	if (m2path == null)
    		m2path = System.getProperty("user.home") + "/.m2";
    	m2dir = new File(m2path);
    	
        key = KeyGenerator.getInstance("AES").generateKey();
    }
    
    @Test
    public void streamSmallJar() throws IOException, InterruptedException, TimeoutException, ExecutionException {
        File jar = new File(m2dir, "repository/org/slf4j/slf4j-api/1.7.30/slf4j-api-1.7.30.jar");
        this.stream(jar, 16384);
        this.stream(jar, 1024);
        this.stream(jar, 32);
        this.stream(jar, 3);
        this.stream(jar, 1);
    }
    
    @Test
    public void streamBigJar() throws IOException, InterruptedException, TimeoutException, ExecutionException {
        File war = new File(m2dir, "repository/junit/junit/4.12/junit-4.12.jar");
        this.stream(war, 16384);
    }
    
    private void stream(final File file, final int bufferSize) throws IOException, InterruptedException, TimeoutException, ExecutionException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 2, 1L, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>(5));
        final int port = 12345;
        
        Future<Void> encrypter = executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                RandomAccessFile rafile = new RandomAccessFile(file, "r");
                FileChannel fchannel = rafile.getChannel();

                Thread.sleep(100L);
                SocketChannel channel = SocketChannel.open();
                Assert.assertTrue(channel.connect(new InetSocketAddress("localhost", port)));
                
                IVEncryptingWritableByteChannel ebchannel = new IVEncryptingWritableByteChannel(channel, key, "AES/CBC/PKCS5Padding");
                
                ByteBuffer bbuffer = ByteBuffer.allocate(bufferSize);
                while (fchannel.read(bbuffer) >= 0) {
                    bbuffer.flip();
                    ebchannel.write(bbuffer);
                    Assert.assertFalse(bbuffer.hasRemaining());
                    bbuffer.compact();
                }
                
                bbuffer.flip();
                while (bbuffer.hasRemaining())
                    ebchannel.write(bbuffer);
                ebchannel.flush();
                
                ebchannel.close();
                channel.close();
                rafile.close();
                return null;
            }
        });

        Future<Void> decrypter = executor.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                RandomAccessFile rafile = new RandomAccessFile(file, "r");
                FileChannel fchannel = rafile.getChannel();

                ServerSocketChannel schannel = ServerSocketChannel.open();
                schannel.socket().bind(new InetSocketAddress(port));
                SocketChannel channel = schannel.accept();
                
                IVDecryptingReadableByteChannel dbchannel = new IVDecryptingReadableByteChannel(channel, key, "AES/CBC/PKCS5Padding");
                
                ByteBuffer bbuffer = ByteBuffer.allocate(bufferSize);
                ByteBuffer fbuffer = ByteBuffer.allocate(bufferSize);
                while (dbchannel.read(bbuffer) >= 0) {
                    bbuffer.flip();
                    fchannel.read(fbuffer);
                    fbuffer.flip();
                    
                    Assert.assertEquals(fbuffer, bbuffer);
                    bbuffer.clear();
                    fbuffer.clear();
                }
                
                dbchannel.close();
                channel.close();
                schannel.close();
                rafile.close();
                return null;
            }
        });
        
        encrypter.get(1L, TimeUnit.HOURS);
        decrypter.get(1L, TimeUnit.HOURS);
        
        executor.shutdown();
        executor.awaitTermination(1L, TimeUnit.MINUTES);
    }

}
