/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.Flushable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

abstract class AbstractCryptoWritableByteChannel extends AbstractCryptoByteChannel implements WritableByteChannel, Flushable {
	
	protected final WritableByteChannel wbchannel;
	
	private int chunkSize = 16384;
	
	public AbstractCryptoWritableByteChannel(WritableByteChannel wbchannel, CipherParameters cparams)
	throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		super(wbchannel, cparams);
		this.wbchannel = wbchannel;
	}
	
	/**
	 * This method overrides the default chunk size for reading this channel
	 * into buffers.  The lower the value, the less memory usage.  The higher
	 * the value, the higher performance.
	 * 
	 * @param chunkSize A number of bytes; defaults to 16KB
	 */
	public void setChunkSize(int chunkSize) {
		this.chunkSize = chunkSize;
	}
    
    /**
     * This method encrypts or decrypts and writes to the underlying channel
     * until the specified buffer is empty.
     * 
     * @param buffer A NIO buffer ready for reading
     * @return The number of bytes written to the underlying channel; never negative
     * @throws IOException An I/O or crypto exception occurred
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    protected int _write(ByteBuffer buffer) throws IOException, IllegalBlockSizeException, BadPaddingException {
    	if (!this.isOpen())
    		return 0;
    	
    	int totalBytesWritten = 0;
    	
    	// ready for writing
    	ByteBuffer writeBuffer = ByteBuffer.allocate(this.chunkSize); 
		
    	while (writeBuffer.hasRemaining() && buffer.hasRemaining() && this.getCipher().stream(buffer, writeBuffer)) {
        	writeBuffer.flip();    // ready for reading
        	totalBytesWritten += this.wbchannel.write(writeBuffer);
        	writeBuffer.compact(); // ready for writing
    	}
    	
    	return totalBytesWritten;
    }
    
    /**
     * This method encrypts or decrypts and writes to the underlying channel
     * until the internal caches and buffers are empty.
     * 
     * @return The number of bytes written to the underlying channel; never negative
     * @throws IOException An I/O or crypto exception occurred
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    protected int _flush() throws IOException, IllegalBlockSizeException, BadPaddingException {
    	if (!this.isOpen())
    		return 0;
    	
    	int totalBytesWritten = 0;
    	
    	// ready for writing
    	ByteBuffer writeBuffer = ByteBuffer.allocate(this.chunkSize); 
		
    	while (writeBuffer.hasRemaining() && this.getCipher().flush(writeBuffer)) {
        	writeBuffer.flip();    // ready for reading
        	totalBytesWritten += this.wbchannel.write(writeBuffer);
        	writeBuffer.compact(); // ready for writing
    	}
    	
    	return totalBytesWritten;
    }
    
    /**
     * This method flushes and closes this channel.
     * 
     * @throws IOException An I/O or crypto exception occurred
     */
    @Override
    public void close() throws IOException {
    	this.flush();
    	super.close();
    }

}
