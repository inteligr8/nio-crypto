package com.inteligr8.nio;

import java.io.File;

import org.junit.Test;

public class Md5DigestUnitTest extends AbstractDigestUnitTest {
	
	@Override
	public String getDefaultAlgorithm() {
		return "MD5";
	}
	
	@Test
	public void shortText() throws Exception {
		this.validateText("Hello", "8b1a9953c4611296a827abf8c47804d7");
	}
	
	@Test
	public void longText() throws Exception {
		this.validateText("Here is some text that is much longer than short.  It is long.  How about that?", "d9c042e20cc3215cda5f7f3648623abb");
	}
	
	@Test
	public void fileText() throws Exception {
		this.validateFile(new File("src/main/java/com/inteligr8/nio/DigestBuffer.java"), "8dac62e518f9e0eb7decfbd890461275");
	}

}
