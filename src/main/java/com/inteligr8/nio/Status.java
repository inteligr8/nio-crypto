/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

/**
 * This enum defines the set of possible status values for a multi-part buffer.
 * Each value is meant to be self-explanatory, but rely on the context of their
 * use.
 * 
 * @author brian@inteligr8.com
 */
@PublicApi
public enum Status {
	
	Uninitialized,
	Initialized,
	Started,
	Finished

}
