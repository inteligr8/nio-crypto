package com.inteligr8.nio;

import java.io.File;

import org.junit.Test;

public class Sha256DigestUnitTest extends AbstractDigestUnitTest {
	
	@Override
	public String getDefaultAlgorithm() {
		return "SHA-256";
	}
	
	@Test
	public void shortText() throws Exception {
		this.validateText("Hello", "185f8db32271fe25f561a6fc938b2e264306ec304eda518007d1764826381969");
	}
	
	@Test
	public void longText() throws Exception {
		this.validateText("Here is some text that is much longer than short.  It is long.  How about that?", "3acef9887592d6f6347bf750d0b63d1d652ec81d88fe0ff2f5f1d7aa6f21dc29");
	}
	
	@Test
	public void fileText() throws Exception {
		this.validateFile(new File("src/main/java/com/inteligr8/nio/DigestBuffer.java"), "c82a52583fc3319b117c6bee33781ef6cafb5058e77236271dbb636ba08ee3f1");
	}

}
