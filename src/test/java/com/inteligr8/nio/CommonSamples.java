package com.inteligr8.nio;

import java.io.BufferedReader;
import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.junit.Test;

public class CommonSamples {
	
	@Test
	public void aes() throws Exception {
		// generate random key using JCE
		int keysize = 256;
		KeyGenerator keygen = KeyGenerator.getInstance("AES");
		keygen.init(keysize);
		SecretKey key = keygen.generateKey();
		
		// prepare file (channel) to receive encrypted conten
		FileChannel fchannel = FileChannel.open(new File("target/test.aes").toPath(),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		try {
			// wrap channel with encrypting channel and convenient string writer
			IVEncryptingWritableByteChannel cryptochannel = new IVEncryptingWritableByteChannel(fchannel, key, "AES/CBC/PKCS5Padding");
			Writer writer = Channels.newWriter(cryptochannel, Charset.defaultCharset().name());
			try {
				writer.append("some plain text to encrypt");
			} finally {
				writer.close();
			}
		} finally {
			fchannel.close();
		}
		
		// prepare file (channel) to deliver encrypted content
		fchannel = FileChannel.open(new File("target/test.aes").toPath(), StandardOpenOption.READ);
		try {
			// wrap channel with decrypting channel and convenient string reader
			IVDecryptingReadableByteChannel cryptochannel = new IVDecryptingReadableByteChannel(fchannel, key, "AES/CBC/PKCS5Padding");
			Reader reader = Channels.newReader(cryptochannel, Charset.defaultCharset().name());
			
			System.out.println(new BufferedReader(reader).readLine());
		} finally {
			fchannel.close();
		}
	}
	
	@Test
	public void rsa() throws Exception {
		// generate random key using JCE
		int keysize = 4096;
		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
		keygen.initialize(keysize);
		KeyPair keys = keygen.generateKeyPair();
		
		// prepare file (channel) to receive encrypted content
		FileChannel fchannel = FileChannel.open(new File("target/test.rsa").toPath(),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		try {
			// wrap channel with encrypting channel and convenient string writer
			EncryptingCipherParameters ecparams = new EncryptingCipherParameters(keys.getPrivate(), "RSA/ECB/PKCS1Padding");
			EncryptingWritableByteChannel cryptochannel = new EncryptingWritableByteChannel(fchannel, ecparams);
			Writer writer = Channels.newWriter(cryptochannel, Charset.defaultCharset().name());
			try {
				writer.append("some plain text to encrypt");
			} finally {
				writer.close();
			}
		} finally {
			fchannel.close();
		}
		
		// prepare file (channel) to deliver encrypted content
		fchannel = FileChannel.open(new File("target/test.rsa").toPath(), StandardOpenOption.READ);
		try {
			// wrap channel with decrypting channel and convenient string reader
			DecryptingCipherParameters edparams = new DecryptingCipherParameters(keys.getPublic(), "RSA/ECB/PKCS1Padding");
			DecryptingReadableByteChannel cryptochannel = new DecryptingReadableByteChannel(fchannel, edparams);
			Reader reader = Channels.newReader(cryptochannel, Charset.defaultCharset().name());
			
			System.out.println(new BufferedReader(reader).readLine());
		} finally {
			fchannel.close();
		}
	}

}
