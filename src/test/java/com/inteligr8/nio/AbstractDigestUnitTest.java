package com.inteligr8.nio;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.DigestException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

abstract class AbstractDigestUnitTest {
	
	public abstract String getDefaultAlgorithm();
	
	protected DigestBuffer createDigest() throws NoSuchAlgorithmException {
		return new DigestBuffer(new DigestParameters(this.getDefaultAlgorithm()));
	}
	
	public void validateText(String text, String hex) throws NoSuchAlgorithmException, DigestException {
		ByteBuffer buffer = ByteBuffer.wrap(text.getBytes(Charset.forName("utf-8")));

		DigestBuffer digest = this.createDigest();
		digest.stream(buffer);
		Assert.assertFalse(buffer.hasRemaining());

		ByteBuffer output = ByteBuffer.allocate(digest.getMessageDigest().getDigestLength());
		while (digest.flush(output))
			;
		
		output.flip();
		Assert.assertEquals(hex, this.buffer2hex(output));
	}
	
	public void validateFile(File file, String hex) throws NoSuchAlgorithmException, IOException, DigestException {
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		DigestBuffer digest = this.createDigest();
		
		FileChannel fchannel = FileChannel.open(file.toPath());
		try {
			while (fchannel.read(buffer) >= 0) {
				buffer.flip();
				digest.stream(buffer);
				buffer.compact();
			}
		} finally {
			fchannel.close();
		}

		ByteBuffer output = ByteBuffer.allocate(digest.getMessageDigest().getDigestLength());
		while (digest.flush(output))
			;
		
		output.flip();
		Assert.assertEquals(hex, this.buffer2hex(output));
	}
	
	private String buffer2hex(ByteBuffer buffer) {
		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);
		return new String(Hex.encodeHex(bytes));
	}
	
	@Test
	public void testEmptyFlushEmptyTargetHash() throws Exception {
		DigestBuffer digest = this.createDigest();
		
		ByteBuffer buffer = ByteBuffer.allocate(0);
		Assert.assertTrue(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(0L, digest.getTotalBytesWritten());

		Assert.assertTrue(digest.flush(buffer));
	}
	
	@Test
	public void testEmptyFlushHash() throws Exception {
		DigestBuffer digest = this.createDigest();
		
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		Assert.assertTrue(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(digest.getMessageDigest().getDigestLength(), digest.getTotalBytesWritten());

		Assert.assertFalse(digest.flush(buffer));
	}
	
	@Test
	public void testEmptyMultiFlushHash() throws Exception {
		DigestBuffer digest = this.createDigest();

		int chunkSize = digest.getMessageDigest().getDigestLength() / 2 - 1;
		ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
		Assert.assertTrue(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(chunkSize, digest.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(chunkSize * 2L, digest.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(digest.getMessageDigest().getDigestLength(), digest.getTotalBytesWritten());
		Assert.assertEquals(2, buffer.position());

		buffer.clear();
		Assert.assertFalse(digest.flush(buffer));
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(digest.getMessageDigest().getDigestLength(), digest.getTotalBytesWritten());
		Assert.assertEquals(0, buffer.position());

		Assert.assertFalse(digest.flush(buffer));
	}
	
	@Test
	public void testEmptyStreamEmptyTargetHash() throws Exception {
		DigestBuffer digest = this.createDigest();

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		digest.stream(sourceBuffer);
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(0L, digest.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyStreamHash() throws Exception {
		DigestBuffer digest = this.createDigest();

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		digest.stream(sourceBuffer);
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(0L, digest.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiStreamHash() throws Exception {
		DigestBuffer digest = this.createDigest();

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		digest.stream(sourceBuffer);
		Assert.assertEquals(0L, digest.getTotalBytesRead());
		Assert.assertEquals(0L, digest.getTotalBytesWritten());

		digest.stream(sourceBuffer);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStreamAfterFlush() throws Exception {
		DigestBuffer digest = this.createDigest();

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertTrue(digest.flush(targetBuffer));
		digest.stream(sourceBuffer);
		Assert.fail();
	}

}
