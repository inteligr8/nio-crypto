package com.inteligr8.nio;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.KeyPair;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.junit.Assert;
import org.junit.Test;

public class AsymmetricBlockCipherUnitTest extends AbstractCipherUnitTest {
	
	@Override
	public String getDefaultAlgorithm() {
		return "RSA";
	}
	
	@Override
	public String getDefaultTransformation() {
		return "RSA/ECB/PKCS1Padding";
	}
	
	@Override
	public int getDefaultKeySize() {
		return 2048;
	}
	
	@Test
	public void testEmptyFlushEmptyTargetEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		
		ByteBuffer buffer = ByteBuffer.allocate(0);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyFlushEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), buffer.position());

		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiFlushEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		int chunkSize = this.getDefaultKeySizeInBytes() / 2 - 1;
		ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize * 2L, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(2, buffer.position());

		buffer.clear();
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyFlushEmptyTargetDecryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		
		ByteBuffer buffer = ByteBuffer.allocate(0);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyFlushDecryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiFlushDecryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		
		int chunkSize = this.getDefaultKeySizeInBytes() / 2 - 1;
		ByteBuffer buffer = ByteBuffer.allocate(chunkSize);
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize*2, cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, buffer.position());

		buffer.clear();
		Assert.assertTrue(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(2, buffer.position());

		buffer.clear();
		Assert.assertFalse(cipher.flush(buffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyStreamEmptyTargetEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyStreamEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiStreamEncryption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
		
		targetBuffer.clear();
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
	}
	
	@Test
	public void testEmptyStreamEmptyTargetDecyption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyStreamDecyption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());
	}
	
	@Test
	public void testEmptyMultiStreamDecyption() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(this.getDefaultKeySizeInBytes());
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(0L, cipher.getTotalBytesRead());
		Assert.assertEquals(0L, cipher.getTotalBytesWritten());

		targetBuffer.clear();
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStreamAfterFlush() throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		ByteBuffer sourceBuffer = ByteBuffer.allocate(0);
		ByteBuffer targetBuffer = ByteBuffer.allocate(0);
		Assert.assertTrue(cipher.flush(targetBuffer));
		cipher.stream(sourceBuffer, targetBuffer);
		Assert.fail();
	}
	
	@Test
	public void testEmptyEncryption() throws Exception {
		this.testXByteEncryption(0);
	}
	
	@Test
	public void test1ByteEncryption() throws Exception {
		this.testXByteEncryption(1);
	}
	
	@Test
	public void test5ByteEncryption() throws Exception {
		this.testXByteEncryption(5);
	}
	
	@Test
	public void test200ByteEncryption() throws Exception {
		this.testXByteEncryption(200);
	}
	
	@Test(expected = IllegalBlockSizeException.class)
	public void testTooManyBytesEncryption() throws Exception {
		this.testXByteEncryption(this.getDefaultKeySizeInBytes()+1);
	}
	
	private void testXByteEncryption(int bytes) throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer cipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());

		int chunkSize = this.getDefaultKeySizeInBytes() / 2;
		
		ByteBuffer sourceBuffer = ByteBuffer.allocate(bytes);
		ByteBuffer targetBuffer = ByteBuffer.allocate(chunkSize);
		Assert.assertFalse(cipher.stream(sourceBuffer, targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(0, cipher.getTotalBytesWritten());
		Assert.assertEquals(bytes, sourceBuffer.position());
		Assert.assertEquals(0, targetBuffer.position());
		
		targetBuffer.clear();
		Assert.assertTrue(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(chunkSize, cipher.getTotalBytesWritten());
		Assert.assertEquals(bytes, sourceBuffer.position());
		Assert.assertEquals(chunkSize, targetBuffer.position());
		
		targetBuffer.clear();
		Assert.assertTrue(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
		Assert.assertEquals(chunkSize, targetBuffer.position());

		targetBuffer.clear();
		Assert.assertFalse(cipher.flush(targetBuffer));
		Assert.assertEquals(bytes, cipher.getTotalBytesRead());
		Assert.assertEquals(this.getDefaultKeySizeInBytes(), cipher.getTotalBytesWritten());
	}
	
	@Test
	public void encryptDecryptShort() throws Exception {
		String plainText = "Short!";
		this.encryptDecrypt(plainText);
	}
	
	@Test
	public void encryptDecryptLong() throws Exception {
		String plainText = "Here is some text that has a respectable amount of length to it";
		this.encryptDecrypt(plainText);
	}
	
	private void encryptDecrypt(String plainText) throws Exception {
		KeyPair keypair = this.generatePublicPrivateKey();
		CipherBuffer encryptionCipher = this.createCipher(Cipher.ENCRYPT_MODE, keypair.getPublic());
		CipherBuffer decryptionCipher = this.createCipher(Cipher.DECRYPT_MODE, keypair.getPrivate(), encryptionCipher);

		ByteBuffer sourceBuffer = ByteBuffer.wrap(plainText.getBytes(Charset.defaultCharset()));
		ByteBuffer encryptedBuffer = ByteBuffer.allocate(1024 * 1024); // plenty of size
		ByteBuffer decryptedBuffer = ByteBuffer.allocate(1024 * 1024); // plenty of size
		while (encryptionCipher.stream(sourceBuffer, encryptedBuffer))
			;
		while (encryptionCipher.flush(encryptedBuffer))
			;
		
		Assert.assertEquals(plainText.length(), encryptionCipher.getTotalBytesRead());
		Assert.assertTrue(encryptionCipher.getTotalBytesRead() < encryptionCipher.getTotalBytesWritten());
		
		encryptedBuffer.flip(); // ready for reading
		Assert.assertEquals(encryptionCipher.getTotalBytesWritten(), encryptedBuffer.remaining());
		
		while (decryptionCipher.stream(encryptedBuffer, decryptedBuffer))
			;
		while (decryptionCipher.flush(decryptedBuffer))
			;
		
		decryptedBuffer.flip(); // ready for reading

		Assert.assertEquals(encryptionCipher.getTotalBytesWritten(), decryptionCipher.getTotalBytesRead());
		Assert.assertEquals(plainText.length(), decryptionCipher.getTotalBytesWritten());
		Assert.assertEquals(plainText, new String(decryptedBuffer.array(), decryptedBuffer.position(), decryptedBuffer.remaining(), Charset.defaultCharset()));
	}

}
