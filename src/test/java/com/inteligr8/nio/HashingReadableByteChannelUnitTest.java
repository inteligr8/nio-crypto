package com.inteligr8.nio;

import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.ReadableByteChannel;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;

public class HashingReadableByteChannelUnitTest extends AbstractHashingByteChannelUnitTest {

	@Override
    public void test(String algorithm, ByteChannel bchannel, String hex) throws Exception {
    	this.test(algorithm, (ReadableByteChannel)bchannel, hex);
    }
    
    public void test(String algorithm, ReadableByteChannel rbchannel, String hex) throws Exception {
        HashingReadableByteChannel hrbchannel = new HashingReadableByteChannel(rbchannel, new DigestParameters(algorithm));
        try {
            ByteBuffer bbuffer = ByteBuffer.allocate(hrbchannel.getHashSize());
            
        	while (hrbchannel.read(bbuffer) >= 0)
        		;
            
            Assert.assertEquals(hrbchannel.getHashSize(), hrbchannel.getHashBytesWritten());
            
            bbuffer.flip();
            byte[] bytes = new byte[bbuffer.remaining()];
            bbuffer.get(bytes);
            Assert.assertEquals(hex, new String(Hex.encodeHex(bytes)));
        } finally {
            hrbchannel.close();
        }
    }

}
